### Регистация собственника
http://localhost:8080/station/pageController?command=REGISTRATION_PAGE&ownerHash=1

### Применение переменных окружения:
* source ~/.bash_profile

### Запуск томката из папки bin:
* ./catalina.sh start
* ./catalina.sh stop

### Деплой проекта
* mvn tomcat7:deploy
