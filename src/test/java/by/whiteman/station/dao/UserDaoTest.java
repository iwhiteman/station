package by.whiteman.station.dao;

import by.whiteman.station.dao.impl.connection.ConnectionPool;
import by.whiteman.station.dao.impl.connection.DBConnectionException;
import by.whiteman.station.entity.User;
import by.whiteman.station.entity.UserRole;
import by.whiteman.station.entity.UserStatus;
import by.whiteman.station.util.BCryptEncoder;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;


public class UserDaoTest {

	private static final UserDao userDao = DaoFactory.getInstance().getUserDao();

	@BeforeClass
	public static void init() throws DBConnectionException {
		ConnectionPool.getInstance().initPoolData(false);
	}

	@AfterClass
	public static void destroy() throws DBConnectionException {
		ConnectionPool.getInstance().dispose();
	}

	@After
	public void rollback() throws SQLException {
		ConnectionPool.getInstance().rollback();
	}

	@Test
	public void createUserTest() throws DaoException {
		final User testUser = createTestUser();

		final Integer userId = userDao.createUser(testUser);
		Assert.assertNotNull(userId);
	}

	@Test
	public void getUserByIdTest() throws DaoException {
		final Integer testUserId = 3;

		final User userById = userDao.getUserById(testUserId);
		Assert.assertNotNull(userById);
		Assert.assertEquals(testUserId, userById.getId());
	}

	@Test
	public void getUserByEmailTest() throws DaoException {
		final String testUserEmail = "labetskii@gmail.com";

		final User userByEmail = userDao.getUserByEmail(testUserEmail);
		Assert.assertNotNull(userByEmail);
		Assert.assertEquals(testUserEmail, userByEmail.getEmail());
	}

	@Test
	public void updateUserTest() throws DaoException {
		final String testUserEmail = "test@gmail.com";
		final Integer testUserId = 3;
		final User testUser = createTestUser();
		testUser.setEmail(testUserEmail);
		testUser.setId(testUserId);

		userDao.updateUser(testUser);
		//чтобы протестировать остальное, нужно установить autoCommit=true
		final User userByEmail = userDao.getUserByEmail(testUserEmail);
		Assert.assertNotNull(userByEmail);
		Assert.assertEquals(testUserEmail, userByEmail.getEmail());
	}

	@Test
	public void deleteUserTest() throws DaoException {
		final int testUserId = 3;

		userDao.deleteUser(testUserId);
		//чтобы протестировать остальное, нужно установить autoCommit=true
		final User userByEmail = userDao.getUserById(testUserId);
		Assert.assertNull(userByEmail);
	}

	@Test
	public void getUsersByRoleTest() throws DaoException {
		final UserRole testUserRole = UserRole.USER;

		final List<User> usersByRole = userDao.getUsersByRole(testUserRole);
		Assert.assertFalse(usersByRole.isEmpty());
		Assert.assertEquals(testUserRole, usersByRole.get(0).getUserRole());
	}

	private User createTestUser() {
		final User user = new User();
		user.setFirstName("Михаил");
		user.setLastName("Лабецкий");
		user.setPhoneNumber("+375297075435");
		user.setEmail("labetskii@gmail.com");
		user.setPasswordHash(BCryptEncoder.getPasswordHash("test_password"));
		user.setUserStatus(UserStatus.STAFF);
		user.setUserRole(UserRole.USER);
		return user;
	}

}
