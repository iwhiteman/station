package by.whiteman.station.dao;

import by.whiteman.station.dao.impl.connection.ConnectionPool;
import by.whiteman.station.dao.impl.connection.DBConnectionException;
import by.whiteman.station.entity.Order;
import by.whiteman.station.entity.OrderHistory;
import by.whiteman.station.entity.OrderStatus;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;


public class OrderDaoTest {

	private static final OrderDao orderDao = DaoFactory.getInstance().getOrderDao();

	@BeforeClass
	public static void init() throws DBConnectionException {
		ConnectionPool.getInstance().initPoolData(false);
	}

	@AfterClass
	public static void destroy() throws DBConnectionException {
		ConnectionPool.getInstance().dispose();
	}

	@After
	public void rollback() throws SQLException {
		ConnectionPool.getInstance().rollback();
	}

	@Test
	public void createOrderTest() throws DaoException {
		final Order testOrder = createTestOrder();
		final Integer userId = 8;

		final Integer orderId = orderDao.createOrder(testOrder, userId);
		Assert.assertNotNull(orderId);
	}

	@Test
	public void getOrdersTest() throws DaoException {
		final Integer carId = 3;

		final List<Order> orders = orderDao.getOrders(carId);
		Assert.assertFalse(orders.isEmpty());
		Assert.assertEquals(carId, orders.get(0).getCarId());
	}

	@Test
	public void getNewOrdersTest() throws DaoException {
		final List<Order> newOrders = orderDao.getNewOrders();
		Assert.assertFalse(newOrders.isEmpty());
	}

	@Test
	public void getOrdersInWorkTest() throws DaoException {
		final List<Order> ordersInWork = orderDao.getOrdersInWork();
		Assert.assertFalse(ordersInWork.isEmpty());
	}

	@Test
	public void getOrderTest() throws DaoException {
		final Integer orderId = 1;

		final Order order = orderDao.getOrder(orderId);
		Assert.assertNotNull(order);
		Assert.assertEquals(orderId, order.getId());
	}

	@Test
	public void updateOrderTest() throws DaoException {
		final Integer managerId = 11;
		final Order testOrder = createTestOrder();
		testOrder.setId(1);
		testOrder.setStatus(OrderStatus.AGREED);
		testOrder.setManagerId(managerId);
		testOrder.setMasterId(12);

		orderDao.updateOrder(testOrder, managerId, true);
	}

	@Test
	public void getOrderHistoryTest() throws DaoException {
		final Integer orderId = 1;

		final List<OrderHistory> orderHistory = orderDao.getOrderHistory(orderId);
		Assert.assertFalse(orderHistory.isEmpty());
	}

	private Order createTestOrder() {
		final Order order = new Order();
		order.setStatus(OrderStatus.OPENED);
		order.setCreateDate(LocalDateTime.now());
		order.setStartDate(LocalDateTime.now().plusDays(2).withHour(13).withMinute(30));
		order.setDescription("- туго крутится руль. (замена рулевого карданчика)\n- масло, фильтра (без топливного)\n- замена левой лампочки Н7, замена патрона правой лампочки");
		order.setCarId(3);
		order.setCarMileage(60000L);
		return order;
	}

}
