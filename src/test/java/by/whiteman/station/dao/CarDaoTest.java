package by.whiteman.station.dao;

import by.whiteman.station.dao.impl.connection.ConnectionPool;
import by.whiteman.station.dao.impl.connection.DBConnectionException;
import by.whiteman.station.entity.Car;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;
import java.util.List;


public class CarDaoTest {

	private static final CarDao carDao = DaoFactory.getInstance().getCarDao();

	@BeforeClass
	public static void init() throws DBConnectionException {
		ConnectionPool.getInstance().initPoolData(false);
	}

	@AfterClass
	public static void destroy() throws DBConnectionException {
		ConnectionPool.getInstance().dispose();
	}

	@After
	public void rollback() throws SQLException {
		ConnectionPool.getInstance().rollback();
	}

	@Test
	public void createCarTest() throws DaoException {
		final Car testCar = createTestCar();

		final Integer carId = carDao.createCar(testCar);
		Assert.assertNotNull(carId);
	}

	@Test
	public void getCarsTest() throws DaoException {
		final Integer userId = 8;

		final List<Car> cars = carDao.getCars(userId);
		Assert.assertFalse(cars.isEmpty());
		Assert.assertEquals(userId, cars.get(0).getUserId());
	}

	@Test
	public void getCarTest() throws DaoException {
		final Integer carId = 2;

		final Car car = carDao.getCar(carId);
		Assert.assertNotNull(car);
		Assert.assertEquals(carId, car.getId());
	}

	@Test
	public void updateCarTest() throws DaoException {
		final Integer carId = 2;
		final Integer testEngineCapacity = 3000;
		final Car testCar = createTestCar();
		testCar.setEngineCapacity(testEngineCapacity);
		testCar.setId(carId);

		carDao.updateCar(testCar);
		final Car car = carDao.getCar(carId);
		Assert.assertNotNull(car);
		Assert.assertEquals(testEngineCapacity, car.getEngineCapacity());
	}

	@Test
	public void hasVinCodeTest() throws DaoException {
		final Integer userId = 8;
		final String vinCode = "LVY992ML6JP008957";
		final boolean hasVinCode = carDao.hasVinCode(userId, vinCode);
		Assert.assertTrue(hasVinCode);
	}

	private Car createTestCar() {
		final Car car = new Car();
		car.setBrand("Volvo");
		car.setModel("S90");
		car.setYear(2017);
		car.setRegNumber("0471HH5");
		car.setEngineCapacity(2000);
		car.setVinCode("LVY992ML6JP008957");
		car.setUserId(8);
		return car;
	}

}
