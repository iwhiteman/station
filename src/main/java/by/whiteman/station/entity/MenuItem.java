package by.whiteman.station.entity;

import java.io.Serializable;
import java.util.Objects;


public class MenuItem implements Serializable {

	private boolean active;
	private String title;
	private String name;

	public MenuItem() {
	}

	public MenuItem(String title, String name) {
		this.title = title;
		this.name = name;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getTitle() {
		return title;
	}

	public String getName() {
		return name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		MenuItem menuItem = (MenuItem) o;
		return title.equals(menuItem.title) && name.equals(menuItem.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(title, name);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName()
				+ "{active=" + active
				+ ", title='" + title + '\''
				+ ", name='" + name + '\''
				+ '}';
	}
}
