package by.whiteman.station.entity;

import java.io.Serializable;


public class Car implements Serializable {

	private Integer id;
	private String brand;
	private String model;
	private Integer year;
	private String regNumber;
	private Integer engineCapacity;
	private String vinCode;
	private Integer userId;

	public Car() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getRegNumber() {
		return regNumber;
	}

	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}

	public Integer getEngineCapacity() {
		return engineCapacity;
	}

	public void setEngineCapacity(Integer engineCapacity) {
		this.engineCapacity = engineCapacity;
	}

	public String getVinCode() {
		return vinCode;
	}

	public void setVinCode(String vinCode) {
		this.vinCode = vinCode;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Car car = (Car) o;
		if (!id.equals(car.id)) {
			return false;
		}
		if (!regNumber.equals(car.regNumber)) {
			return false;
		}
		return vinCode.equals(car.vinCode);
	}

	@Override
	public int hashCode() {
		int result = id.hashCode();
		result = 31 * result + regNumber.hashCode();
		result = 31 * result + vinCode.hashCode();
		return result;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName()
				+ "{id=" + id
				+ ", brand='" + brand + '\''
				+ ", model='" + model + '\''
				+ ", year=" + year
				+ ", regNumber='" + regNumber + '\''
				+ ", engineCapacity='" + engineCapacity + '\''
				+ ", vinCode='" + vinCode + '\''
				+ ", userId=" + userId
				+ '}';
	}
}
