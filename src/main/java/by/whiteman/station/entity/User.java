package by.whiteman.station.entity;

import java.io.Serializable;


public class User implements Serializable {

	private Integer id;
	private UserRole userRole;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private String email;
	private String passwordHash;
	private UserStatus userStatus = UserStatus.STAFF;

	public User() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public UserStatus getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(UserStatus userStatus) {
		this.userStatus = userStatus;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		User user = (User) o;
		return id.equals(user.id);
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public String toString() {
		return getClass().getSimpleName()
				+ "{id=" + id
				+ ", role=" + userRole
				+ ", firstName='" + firstName + '\''
				+ ", lastName='" + lastName + '\''
				+ ", phoneNumber='" + phoneNumber + '\''
				+ ", email='" + email + '\''
				+ ", passwordHash='" + passwordHash + '\''
				+ ", userStatus='" + userStatus + '\''
				+ '}';
	}

}
