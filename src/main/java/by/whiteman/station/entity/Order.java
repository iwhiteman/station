package by.whiteman.station.entity;

import java.io.Serializable;
import java.time.LocalDateTime;


public class Order implements Serializable {

	private Integer id;
	private OrderStatus status;
	private LocalDateTime createDate;
	private LocalDateTime startDate;
	private LocalDateTime endDate;
	private String description;
	private String masterNotes;
	private Integer price;

	private Integer carId;
	private Long carMileage;
	private Integer masterId;
	private Integer managerId;

	public Order() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMasterNotes() {
		return masterNotes;
	}

	public void setMasterNotes(String masterNotes) {
		this.masterNotes = masterNotes;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getCarId() {
		return carId;
	}

	public void setCarId(Integer carId) {
		this.carId = carId;
	}

	public Long getCarMileage() {
		return carMileage;
	}

	public void setCarMileage(Long carMileage) {
		this.carMileage = carMileage;
	}

	public Integer getMasterId() {
		return masterId;
	}

	public void setMasterId(Integer masterId) {
		this.masterId = masterId;
	}

	public Integer getManagerId() {
		return managerId;
	}

	public void setManagerId(Integer managerId) {
		this.managerId = managerId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Order order = (Order) o;
		return id.equals(order.id);
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public String toString() {
		return getClass().getSimpleName()
				+ "{id=" + id
				+ ", status=" + status
				+ ", createDate=" + createDate
				+ ", startDate=" + startDate
				+ ", endDate=" + endDate
				+ ", description='" + description + '\''
				+ ", masterNotes='" + masterNotes + '\''
				+ ", price=" + price
				+ ", carId=" + carId
				+ ", carMileage=" + carMileage
				+ ", masterId=" + masterId
				+ ", managerId=" + managerId
				+ '}';
	}
}
