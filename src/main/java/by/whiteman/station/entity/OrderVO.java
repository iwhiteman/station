package by.whiteman.station.entity;

import java.io.Serializable;
import java.util.List;


public class OrderVO implements Serializable {

	private Order order;
	private List<OrderHistory> orderHistory;
	private Car car;
	private User client;

	public OrderVO() {
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public List<OrderHistory> getOrderHistory() {
		return orderHistory;
	}

	public void setOrderHistory(List<OrderHistory> orderHistory) {
		this.orderHistory = orderHistory;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public User getClient() {
		return client;
	}

	public void setClient(User client) {
		this.client = client;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		OrderVO orderVO = (OrderVO) o;
		return order.equals(orderVO.order);
	}

	@Override
	public int hashCode() {
		return order.hashCode();
	}

	@Override
	public String toString() {
		return getClass().getSimpleName()
				+ "{order=" + order
				+ ", orderHistory=" + orderHistory
				+ ", car=" + car
				+ ", client=" + client
				+ '}';
	}

}
