package by.whiteman.station.entity;

public enum UserStatus {

	/**
	 * в штате
	 */
	STAFF(1),

	/**
	 * уволен
	 */
	DISMISSED(2);

	private final int id;

	UserStatus(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public static UserStatus valueOf(int id) {
		for (UserStatus status : values()) {
			if (status.getId() == id) {
				return status;
			}
		}
		throw new IllegalArgumentException(UserStatus.class.getName() + ": not found Enum by ID = " + id);
	}
}
