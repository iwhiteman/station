package by.whiteman.station.entity;

public enum OrderStatus {

	/**
	 * Открыт
	 */
	OPENED(1),

	/**
	 * Согласован
	 */
	AGREED(2),

	/**
	 * В работе
	 */
	IN_WORK(3),

	/**
	 * Ожидание (например, зч)
	 */
	WAITING(4),

	/**
	 * Выполнен
	 */
	COMPLETED(5),

	/**
	 * Закрыт
	 */
	CLOSED(6),

	/**
	 * Отменен
	 */
	CANCELED(7);

	private final int id;

	OrderStatus(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public static OrderStatus valueOf(int id) {
		for (OrderStatus status : values()) {
			if (status.getId() == id) {
				return status;
			}
		}
		throw new IllegalArgumentException(OrderStatus.class.getName() + ": not found Enum by ID = " + id);
	}

}
