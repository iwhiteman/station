package by.whiteman.station.entity;

import java.io.Serializable;
import java.time.LocalDateTime;


public class OrderHistory implements Serializable {

	private Integer id;
	private Integer orderId;
	private User user;
	private OrderStatus orderStatus;
	private LocalDateTime date;
	private String notes;

	public OrderHistory() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		OrderHistory that = (OrderHistory) o;
		return id.equals(that.id);
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public String toString() {
		return getClass().getSimpleName()
				+ "{id=" + id
				+ ", orderId=" + orderId
				+ ", user=" + user
				+ ", orderStatus=" + orderStatus
				+ ", date=" + date
				+ ", notes='" + notes + '\''
				+ '}';
	}
}
