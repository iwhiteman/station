package by.whiteman.station.entity;

public enum UserRole {

	OWNER(1, "role.owner"),
	MANAGER(2, "role.manager"),
	MASTER(3, "role.master"),
	USER(4, "role.user");

	private final int id;
	private final String name;

	UserRole(int roleId, String roleName) {
		this.id = roleId;
		this.name = roleName;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public static UserRole valueOf(int id) {
		for (UserRole role : values()) {
			if (role.getId() == id) {
				return role;
			}
		}
		throw new IllegalArgumentException(UserRole.class.getName() + ": not found Enum by ID = " + id);
	}

}
