package by.whiteman.station.service;

import by.whiteman.station.entity.User;
import by.whiteman.station.entity.UserRole;

import java.util.List;


public interface UserService {

	void createUser(User user) throws ServiceException;

	User getUserById(int id) throws ServiceException;

	User getUserByEmail(String email) throws ServiceException;

	void updateUser(User user) throws ServiceException;

	void deleteUser(int id) throws ServiceException;

	List<User> getUsersByRole(UserRole role) throws ServiceException;

	User validateCredentials(String email, String password) throws ServiceException;

}
