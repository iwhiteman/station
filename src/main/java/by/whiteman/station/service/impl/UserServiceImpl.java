package by.whiteman.station.service.impl;

import by.whiteman.station.dao.DaoException;
import by.whiteman.station.dao.DaoFactory;
import by.whiteman.station.dao.UserDao;
import by.whiteman.station.entity.User;
import by.whiteman.station.entity.UserRole;
import by.whiteman.station.service.ServiceException;
import by.whiteman.station.service.UserService;
import by.whiteman.station.util.BCryptEncoder;

import java.util.List;


public class UserServiceImpl implements UserService {

	private static final UserDao userDao = DaoFactory.getInstance().getUserDao();

	@Override
	public void createUser(User user) throws ServiceException {
		try {
			user.setId(userDao.createUser(user));
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public User getUserById(int id) throws ServiceException {
		try {
			return userDao.getUserById(id);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public User getUserByEmail(String email) throws ServiceException {
		try {
			return userDao.getUserByEmail(email);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void updateUser(User user) throws ServiceException {
		try {
			userDao.updateUser(user);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteUser(int id) throws ServiceException {
		try {
			userDao.deleteUser(id);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<User> getUsersByRole(UserRole role) throws ServiceException {
		try {
			return userDao.getUsersByRole(role);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public User validateCredentials(String email, String password) throws ServiceException {
		try {
			final User userByEmail = userDao.getUserByEmail(email);
			if (userByEmail != null && BCryptEncoder.verifyPassword(password, userByEmail.getPasswordHash())) {
				return userByEmail;
			}
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
		return null;
	}

}
