package by.whiteman.station.service.impl;

import by.whiteman.station.dao.DaoException;
import by.whiteman.station.dao.DaoFactory;
import by.whiteman.station.dao.OrderDao;
import by.whiteman.station.entity.Order;
import by.whiteman.station.entity.OrderHistory;
import by.whiteman.station.service.OrderService;
import by.whiteman.station.service.ServiceException;

import java.util.List;


public final class OrderServiceImpl implements OrderService {

	private static final OrderDao orderDao = DaoFactory.getInstance().getOrderDao();

	@Override
	public void createOrder(Order order, Integer userId) throws ServiceException {
		try {
			order.setId(orderDao.createOrder(order, userId));
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Order> getOrders(Integer carId) throws ServiceException {
		try {
			return orderDao.getOrders(carId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Order> getNewOrders() throws ServiceException {
		try {
			return orderDao.getNewOrders();
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Order> getOrdersInWork() throws ServiceException {
		try {
			return orderDao.getOrdersInWork();
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Order getOrder(Integer orderId) throws ServiceException {
		try {
			return orderDao.getOrder(orderId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void updateOrder(Order order, Integer userId, boolean addOrderHistory) throws ServiceException {
		if (order.getMasterId() == 0) {
			throw new ServiceException("Не выбран мастер из списка");
		}
		try {
			orderDao.updateOrder(order, userId, addOrderHistory);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<OrderHistory> getOrderHistory(Integer orderId) throws ServiceException {
		try {
			return orderDao.getOrderHistory(orderId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

}
