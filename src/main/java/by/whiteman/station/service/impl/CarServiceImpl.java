package by.whiteman.station.service.impl;

import by.whiteman.station.dao.CarDao;
import by.whiteman.station.dao.DaoException;
import by.whiteman.station.dao.DaoFactory;
import by.whiteman.station.entity.Car;
import by.whiteman.station.service.CarService;
import by.whiteman.station.service.ServiceException;

import java.util.List;


public class CarServiceImpl implements CarService {

	private static final CarDao carDao = DaoFactory.getInstance().getCarDao();

	@Override
	public void createCar(Car car) throws ServiceException {
		try {
			car.setId(carDao.createCar(car));
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<Car> getCars(Integer userId) throws ServiceException {
		try {
			return carDao.getCars(userId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Car getCar(Integer carId) throws ServiceException {
		try {
			return carDao.getCar(carId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void updateCar(Car car) throws ServiceException {
		try {
			carDao.updateCar(car);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public boolean hasVinCode(Integer userId, String vinCode) throws ServiceException {
		try {
			return carDao.hasVinCode(userId, vinCode);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

}
