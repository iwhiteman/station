package by.whiteman.station.service;

import by.whiteman.station.entity.Order;
import by.whiteman.station.entity.OrderHistory;

import java.util.List;


public interface OrderService {

	void createOrder(Order order, Integer userId) throws ServiceException;

	List<Order> getOrders(Integer carId) throws ServiceException;

	List<Order> getNewOrders() throws ServiceException;

	List<Order> getOrdersInWork() throws ServiceException;

	Order getOrder(Integer orderId) throws ServiceException;

	void updateOrder(Order order, Integer userId, boolean addOrderHistory) throws ServiceException;

	List<OrderHistory> getOrderHistory(Integer orderId) throws ServiceException;

}
