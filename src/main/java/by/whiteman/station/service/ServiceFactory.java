package by.whiteman.station.service;

import by.whiteman.station.service.impl.CarServiceImpl;
import by.whiteman.station.service.impl.OrderServiceImpl;
import by.whiteman.station.service.impl.UserServiceImpl;


public final class ServiceFactory {

	private static volatile ServiceFactory INSTANCE = null;

	private final UserService userService = new UserServiceImpl();
	private final CarService carService = new CarServiceImpl();
	private final OrderService orderService = new OrderServiceImpl();

	private ServiceFactory() {
	}

	public static ServiceFactory getInstance() {
		ServiceFactory factory = INSTANCE;
		if (factory == null) {
			synchronized (ServiceFactory.class) {
				factory = INSTANCE;
				if (factory == null) {
					INSTANCE = factory = new ServiceFactory();
				}
			}
		}
		return factory;
	}

	public UserService getUserService() {
		return userService;
	}

	public CarService getCarService() {
		return carService;
	}

	public OrderService getOrderService() {
		return orderService;
	}

}

