package by.whiteman.station.service;

import by.whiteman.station.entity.Car;

import java.util.List;


public interface CarService {

	void createCar(Car car) throws ServiceException;

	List<Car> getCars(Integer userId) throws ServiceException;

	Car getCar(Integer carId) throws ServiceException;

	void updateCar(Car car) throws ServiceException;

	boolean hasVinCode(Integer userId, String vinCode) throws ServiceException;

}
