package by.whiteman.station.dao;

import by.whiteman.station.entity.Car;

import java.util.List;


public interface CarDao {

	Integer createCar(Car car) throws DaoException;

	List<Car> getCars(Integer userId) throws DaoException;

	Car getCar(Integer carId) throws DaoException;

	void updateCar(Car car) throws DaoException;

	boolean hasVinCode(Integer userId, String vinCode) throws DaoException;

}
