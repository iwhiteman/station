package by.whiteman.station.dao;

import by.whiteman.station.entity.Order;
import by.whiteman.station.entity.OrderHistory;

import java.util.List;


public interface OrderDao {

	Integer createOrder(Order order, Integer userId) throws DaoException;

	List<Order> getOrders(Integer carId) throws DaoException;

	List<Order> getNewOrders() throws DaoException;

	List<Order> getOrdersInWork() throws DaoException;

	Order getOrder(Integer orderId) throws DaoException;

	void updateOrder(Order order, Integer userId, boolean addOrderHistory) throws DaoException;

	List<OrderHistory> getOrderHistory(Integer orderId) throws DaoException;

}
