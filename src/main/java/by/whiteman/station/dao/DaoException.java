package by.whiteman.station.dao;

public class DaoException extends Exception {

	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}

	public DaoException(Throwable cause) {
		super(cause);
	}

}
