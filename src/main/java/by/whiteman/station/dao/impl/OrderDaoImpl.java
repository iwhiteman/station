package by.whiteman.station.dao.impl;

import by.whiteman.station.dao.DaoException;
import by.whiteman.station.dao.OrderDao;
import by.whiteman.station.dao.impl.connection.ConnectionPool;
import by.whiteman.station.dao.impl.connection.DBConnectionException;
import by.whiteman.station.entity.Order;
import by.whiteman.station.entity.OrderHistory;
import by.whiteman.station.entity.OrderStatus;
import by.whiteman.station.entity.User;
import by.whiteman.station.entity.UserRole;
import by.whiteman.station.entity.UserStatus;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


public class OrderDaoImpl implements OrderDao {

	private static final Logger logger = LogManager.getLogger(OrderDaoImpl.class);

	private static final ConnectionPool connectionPool = ConnectionPool.getInstance();

	private static final String createOrderQuery = "INSERT INTO station.orders (create_date, start_date, mileage, description, order_status_id, car_id) VALUES (?,?,?,?,?,?)";
	private static final String createOrderHistoryQuery = "INSERT INTO station.orders_progress (date, order_id, order_status_id, user_id) VALUES (?,?,?,?)";
	private static final String getOrdersQuery = "SELECT * FROM station.orders WHERE car_id=?";
	private static final String getNewOrdersQuery = "SELECT * FROM station.orders WHERE order_status_id=? ORDER BY create_date";
	private static final String getOrdersInWorkQuery = "SELECT * FROM station.orders WHERE order_status_id NOT IN (?,?,?) ORDER BY start_date";
	private static final String getOrderQuery = "SELECT * FROM station.orders WHERE id=?";
	private static final String updateOrderQuery = "UPDATE station.orders SET start_date=?, end_date=?, mileage=?, description=?, master_notes=?, price=?, order_status_id=?, master_id=?, manager_id=? WHERE id=?";
	private static final String getOrderHistoryQuery = "SELECT * FROM station.orders_progress JOIN station.users ON orders_progress.user_id=users.id WHERE order_id=? ORDER BY date DESC";

	@Override
	public Integer createOrder(Order order, Integer userId) throws DaoException {
		try (final Connection connection = connectionPool.takeConnection();
			 final PreparedStatement statement = connection.prepareStatement(createOrderQuery, Statement.RETURN_GENERATED_KEYS);
			 final PreparedStatement historyStatement = connection.prepareStatement(createOrderHistoryQuery)) {
			statement.setTimestamp(1, Timestamp.valueOf(order.getCreateDate()));
			statement.setTimestamp(2, Timestamp.valueOf(order.getStartDate()));
			statement.setLong(3, order.getCarMileage());
			statement.setString(4, order.getDescription());
			statement.setInt(5, order.getStatus().getId());
			statement.setInt(6, order.getCarId());
			statement.executeUpdate();
			final ResultSet resultSet = statement.getGeneratedKeys();
			final Integer orderId;
			if (resultSet.next()) {
				orderId = resultSet.getInt(1);
			} else {
				orderId = null;
			}
			if (orderId != null) {
				addOrderHistory(historyStatement, orderId, OrderStatus.OPENED, userId);
			}
			return orderId;
		} catch (SQLException | DBConnectionException e) {
			logger.error(e);
			throw new DaoException(e);
		}
	}

	@Override
	public List<Order> getOrders(Integer carId) throws DaoException {
		try (final Connection connection = connectionPool.takeConnection();
			 final PreparedStatement statement = connection.prepareStatement(getOrdersQuery)) {
			statement.setInt(1, carId);
			statement.executeQuery();
			final ResultSet resultSet = statement.getResultSet();
			List<Order> ordersList = new ArrayList<>();
			while (resultSet.next()) {
				ordersList.add(createOrderByResultSet(resultSet));
			}
			return ordersList;
		} catch (SQLException | DBConnectionException e) {
			logger.error(e);
			throw new DaoException(e);
		}
	}

	@Override
	public List<Order> getNewOrders() throws DaoException {
		try (final Connection connection = connectionPool.takeConnection();
			 final PreparedStatement statement = connection.prepareStatement(getNewOrdersQuery)) {
			statement.setInt(1, OrderStatus.OPENED.getId());
			statement.executeQuery();
			final ResultSet resultSet = statement.getResultSet();
			List<Order> ordersList = new ArrayList<>();
			while (resultSet.next()) {
				ordersList.add(createOrderByResultSet(resultSet));
			}
			return ordersList;
		} catch (SQLException | DBConnectionException e) {
			logger.error(e);
			throw new DaoException(e);
		}
	}

	@Override
	public List<Order> getOrdersInWork() throws DaoException {
		try (final Connection connection = connectionPool.takeConnection();
			 final PreparedStatement statement = connection.prepareStatement(getOrdersInWorkQuery)) {
			statement.setInt(1, OrderStatus.OPENED.getId());
			statement.setInt(2, OrderStatus.CLOSED.getId());
			statement.setInt(3, OrderStatus.CANCELED.getId());
			statement.executeQuery();
			final ResultSet resultSet = statement.getResultSet();
			List<Order> ordersList = new ArrayList<>();
			while (resultSet.next()) {
				ordersList.add(createOrderByResultSet(resultSet));
			}
			return ordersList;
		} catch (SQLException | DBConnectionException e) {
			logger.error(e);
			throw new DaoException(e);
		}
	}

	@Override
	public Order getOrder(Integer orderId) throws DaoException {
		try (final Connection connection = connectionPool.takeConnection();
			 final PreparedStatement statement = connection.prepareStatement(getOrderQuery)) {
			statement.setInt(1, orderId);
			statement.executeQuery();
			final ResultSet resultSet = statement.getResultSet();
			if (resultSet.next()) {
				return createOrderByResultSet(resultSet);
			} else {
				return null;
			}
		} catch (SQLException | DBConnectionException e) {
			logger.error(e);
			throw new DaoException(e);
		}
	}

	@Override
	public void updateOrder(Order order, Integer userId, boolean addOrderHistory) throws DaoException {
		try (final Connection connection = connectionPool.takeConnection();
			 final PreparedStatement statement = connection.prepareStatement(updateOrderQuery);
			 final PreparedStatement historyStatement = connection.prepareStatement(createOrderHistoryQuery)) {
			statement.setTimestamp(1, Timestamp.valueOf(order.getStartDate()));
			final LocalDateTime orderEndDate = order.getEndDate();
			statement.setTimestamp(2, orderEndDate == null ? null : Timestamp.valueOf(orderEndDate));
			statement.setLong(3, order.getCarMileage());
			statement.setString(4, order.getDescription());
			statement.setString(5, order.getMasterNotes());
			statement.setObject(6, order.getPrice());
			statement.setInt(7, order.getStatus().getId());
			statement.setObject(8, order.getMasterId());
			final Integer managerId = order.getManagerId();
			statement.setObject(9, managerId);
			statement.setInt(10, order.getId());
			statement.executeUpdate();

			if (addOrderHistory) {
				addOrderHistory(historyStatement, order.getId(), order.getStatus(), userId);
			}
		} catch (SQLException | DBConnectionException e) {
			logger.error(e);
			throw new DaoException(e);
		}
	}

	@Override
	public List<OrderHistory> getOrderHistory(Integer orderId) throws DaoException {
		try (final Connection connection = connectionPool.takeConnection();
			 final PreparedStatement statement = connection.prepareStatement(getOrderHistoryQuery)) {
			statement.setInt(1, orderId);
			statement.executeQuery();
			final ResultSet resultSet = statement.getResultSet();
			List<OrderHistory> orderHistoryList = new ArrayList<>();
			while (resultSet.next()) {
				final OrderHistory orderHistory = new OrderHistory();
				orderHistory.setId(resultSet.getInt(1));
				orderHistory.setDate(resultSet.getTimestamp(2).toLocalDateTime());
				orderHistory.setNotes(resultSet.getString(3));
				orderHistory.setOrderId(resultSet.getInt(4));
				orderHistory.setOrderStatus(OrderStatus.valueOf(resultSet.getInt(5)));
				final User user = new User();
				user.setId(resultSet.getInt(7));
				user.setFirstName(resultSet.getString(8));
				user.setLastName(resultSet.getString(9));
				user.setPhoneNumber(resultSet.getString(10));
				user.setEmail(resultSet.getString(11));
				user.setPasswordHash(resultSet.getString(12));
				user.setUserStatus(UserStatus.valueOf(resultSet.getInt(13)));
				user.setUserRole(UserRole.valueOf(resultSet.getInt(14)));
				orderHistory.setUser(user);
				orderHistoryList.add(orderHistory);
			}
			return orderHistoryList;
		} catch (SQLException | DBConnectionException e) {
			logger.error(e);
			throw new DaoException(e);
		}
	}

	private void addOrderHistory(PreparedStatement statement, Integer orderId, OrderStatus orderStatus, Integer userId) throws SQLException {
		statement.setTimestamp(1, Timestamp.valueOf(LocalDateTime.now()));
		statement.setInt(2, orderId);
		statement.setInt(3, orderStatus.getId());
		statement.setInt(4, userId);
		statement.execute();
	}

	private Order createOrderByResultSet(ResultSet rs) throws SQLException {
		final Order order = new Order();
		order.setId(rs.getInt(1));
		order.setCreateDate(rs.getTimestamp(2).toLocalDateTime());
		order.setStartDate(rs.getTimestamp(3).toLocalDateTime());
		final Timestamp endDateTimestamp = rs.getTimestamp(4);
		if (endDateTimestamp != null) {
			order.setEndDate(endDateTimestamp.toLocalDateTime());
		}
		order.setCarMileage(rs.getLong(5));
		order.setDescription(rs.getString(6));
		order.setMasterNotes(rs.getString(7));
		order.setPrice(rs.getInt(8));
		order.setStatus(OrderStatus.valueOf(rs.getInt(9)));
		order.setCarId(rs.getInt(10));
		order.setMasterId(rs.getInt(11));
		order.setManagerId(rs.getInt(12));
		return order;
	}

}
