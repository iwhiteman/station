package by.whiteman.station.dao.impl;

import by.whiteman.station.dao.DaoException;
import by.whiteman.station.dao.UserDao;
import by.whiteman.station.dao.impl.connection.ConnectionPool;
import by.whiteman.station.dao.impl.connection.DBConnectionException;
import by.whiteman.station.entity.User;
import by.whiteman.station.entity.UserRole;
import by.whiteman.station.entity.UserStatus;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class UserDaoImpl implements UserDao {

	private static final Logger logger = LogManager.getLogger(UserDaoImpl.class);

	private static final ConnectionPool connectionPool = ConnectionPool.getInstance();

	private static final String createUserQuery = "INSERT INTO station.users (first_name, last_name, phone_number, email, password_hash, user_status_id, user_role_id) VALUES (?,?,?,?,?,?,?)";
	private static final String getUserByIdQuery = "SELECT * FROM station.users WHERE id=? AND NOT user_status_id=?";
	private static final String getUserByEmailQuery = "SELECT * FROM station.users WHERE email=? AND NOT user_status_id=?";
	private static final String updateUserQuery = "UPDATE station.users SET first_name=?, last_name=?, phone_number=?, email=?, password_hash=? WHERE id=?";
	private static final String deleteUserQuery = "UPDATE station.users SET user_status_id=? WHERE id=?";
	private static final String getUsersByRoleQuery = "SELECT * FROM station.users WHERE user_role_id=? AND NOT user_status_id=?";

	@Override
	public Integer createUser(User user) throws DaoException {
		try (final Connection connection = connectionPool.takeConnection();
			 final PreparedStatement statement = connection.prepareStatement(createUserQuery, Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, user.getFirstName());
			statement.setString(2, user.getLastName());
			statement.setString(3, user.getPhoneNumber());
			statement.setString(4, user.getEmail());
			statement.setString(5, user.getPasswordHash());
			statement.setInt(6, user.getUserStatus().getId());
			statement.setInt(7, user.getUserRole().getId());
			statement.executeUpdate();
			final ResultSet resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getInt(1);
			} else {
				return null;
			}
		} catch (SQLException | DBConnectionException e) {
			logger.error(e);
			throw new DaoException(e);
		}
	}

	@Override
	public User getUserById(int id) throws DaoException {
		try (final Connection connection = connectionPool.takeConnection();
			 final PreparedStatement statement = connection.prepareStatement(getUserByIdQuery)) {
			statement.setInt(1, id);
			statement.setInt(2, UserStatus.DISMISSED.getId());
			statement.executeQuery();
			final ResultSet resultSet = statement.getResultSet();
			if (resultSet.next()) {
				return createUserByResultSet(resultSet);
			} else {
				return null;
			}
		} catch (SQLException | DBConnectionException e) {
			logger.error(e);
			throw new DaoException(e);
		}
	}

	@Override
	public User getUserByEmail(String email) throws DaoException {
		try (final Connection connection = connectionPool.takeConnection();
			 final PreparedStatement statement = connection.prepareStatement(getUserByEmailQuery)) {
			statement.setString(1, email);
			statement.setInt(2, UserStatus.DISMISSED.getId());
			statement.executeQuery();
			final ResultSet resultSet = statement.getResultSet();
			if (resultSet.next()) {
				return createUserByResultSet(resultSet);
			} else {
				return null;
			}
		} catch (SQLException | DBConnectionException e) {
			logger.error(e);
			throw new DaoException(e);
		}
	}

	@Override
	public void updateUser(User user) throws DaoException {
		try (final Connection connection = connectionPool.takeConnection();
			 final PreparedStatement statement = connection.prepareStatement(updateUserQuery)) {
			statement.setString(1, user.getFirstName());
			statement.setString(2, user.getLastName());
			statement.setString(3, user.getPhoneNumber());
			statement.setString(4, user.getEmail());
			statement.setString(5, user.getPasswordHash());
			statement.setInt(6, user.getId());
			statement.executeUpdate();
		} catch (SQLException | DBConnectionException e) {
			logger.error(e);
			throw new DaoException(e);
		}
	}

	@Override
	public void deleteUser(int id) throws DaoException {
		try (final Connection connection = connectionPool.takeConnection();
			 final PreparedStatement statement = connection.prepareStatement(deleteUserQuery)) {
			statement.setInt(1, UserStatus.DISMISSED.getId());
			statement.setInt(2, id);
			statement.executeUpdate();
		} catch (SQLException | DBConnectionException e) {
			logger.error(e);
			throw new DaoException(e);
		}
	}

	@Override
	public List<User> getUsersByRole(UserRole role) throws DaoException {
		try (final Connection connection = connectionPool.takeConnection();
			 final PreparedStatement statement = connection.prepareStatement(getUsersByRoleQuery)) {
			statement.setInt(1, role.getId());
			statement.setInt(2, UserStatus.DISMISSED.getId());
			statement.executeQuery();
			final ResultSet resultSet = statement.getResultSet();
			List<User> usersList = new ArrayList<>();
			while (resultSet.next()) {
				usersList.add(createUserByResultSet(resultSet));
			}
			return usersList;
		} catch (SQLException | DBConnectionException e) {
			logger.error(e);
			throw new DaoException(e);
		}
	}

	private User createUserByResultSet(ResultSet rs) throws SQLException {
		final User user = new User();
		user.setId(rs.getInt(1));
		user.setFirstName(rs.getString(2));
		user.setLastName(rs.getString(3));
		user.setPhoneNumber(rs.getString(4));
		user.setEmail(rs.getString(5));
		user.setPasswordHash(rs.getString(6));
		user.setUserStatus(UserStatus.valueOf(rs.getInt(7)));
		user.setUserRole(UserRole.valueOf(rs.getInt(8)));
		return user;
	}

}
