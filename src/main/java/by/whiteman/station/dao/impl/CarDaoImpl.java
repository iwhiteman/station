package by.whiteman.station.dao.impl;

import by.whiteman.station.dao.CarDao;
import by.whiteman.station.dao.DaoException;
import by.whiteman.station.dao.impl.connection.ConnectionPool;
import by.whiteman.station.dao.impl.connection.DBConnectionException;
import by.whiteman.station.entity.Car;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class CarDaoImpl implements CarDao {

	private static final Logger logger = LogManager.getLogger(CarDaoImpl.class);

	private static final ConnectionPool connectionPool = ConnectionPool.getInstance();

	private static final String createCarQuery = "INSERT INTO station.cars (brand, model, year, reg_number, engine_capacity, vin_code, user_id) VALUES (?,?,?,?,?,?,?)";
	private static final String getCarsQuery = "SELECT * FROM station.cars WHERE user_id=?";
	private static final String getCarQuery = "SELECT * FROM station.cars WHERE id=?";
	private static final String updateCarQuery = "UPDATE station.cars SET brand=?, model=?, year=?, reg_number=?, engine_capacity=?, vin_code=? WHERE id=?";
	private static final String hasVinCodeQuery = "SELECT * FROM station.cars WHERE user_id=? AND vin_code=?";

	@Override
	public Integer createCar(Car car) throws DaoException {
		try (final Connection connection = connectionPool.takeConnection();
			 final PreparedStatement statement = connection.prepareStatement(createCarQuery, Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, car.getBrand());
			statement.setString(2, car.getModel());
			statement.setInt(3, car.getYear());
			statement.setString(4, car.getRegNumber());
			statement.setInt(5, car.getEngineCapacity());
			statement.setString(6, car.getVinCode());
			statement.setInt(7, car.getUserId());
			statement.executeUpdate();
			final ResultSet resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				return resultSet.getInt(1);
			} else {
				return null;
			}
		} catch (SQLException | DBConnectionException e) {
			logger.error(e);
			throw new DaoException(e);
		}
	}

	@Override
	public List<Car> getCars(Integer userId) throws DaoException {
		try (final Connection connection = connectionPool.takeConnection();
			 final PreparedStatement statement = connection.prepareStatement(getCarsQuery)) {
			statement.setInt(1, userId);
			statement.executeQuery();
			final ResultSet resultSet = statement.getResultSet();
			List<Car> carsList = new ArrayList<>();
			while (resultSet.next()) {
				carsList.add(createCarByResultSet(resultSet));
			}
			return carsList;
		} catch (SQLException | DBConnectionException e) {
			logger.error(e);
			throw new DaoException(e);
		}
	}

	@Override
	public Car getCar(Integer carId) throws DaoException {
		try (final Connection connection = connectionPool.takeConnection();
			 final PreparedStatement statement = connection.prepareStatement(getCarQuery)) {
			statement.setInt(1, carId);
			statement.executeQuery();
			final ResultSet resultSet = statement.getResultSet();
			if (resultSet.next()) {
				return createCarByResultSet(resultSet);
			} else {
				return null;
			}
		} catch (SQLException | DBConnectionException e) {
			logger.error(e);
			throw new DaoException(e);
		}
	}

	@Override
	public void updateCar(Car car) throws DaoException {
		try (final Connection connection = connectionPool.takeConnection();
			 final PreparedStatement statement = connection.prepareStatement(updateCarQuery)) {
			statement.setString(1, car.getBrand());
			statement.setString(2, car.getModel());
			statement.setInt(3, car.getYear());
			statement.setString(4, car.getRegNumber());
			statement.setInt(5, car.getEngineCapacity());
			statement.setString(6, car.getVinCode());
			statement.setInt(7, car.getId());
			statement.executeUpdate();
		} catch (SQLException | DBConnectionException e) {
			logger.error(e);
			throw new DaoException(e);
		}
	}

	@Override
	public boolean hasVinCode(Integer userId, String vinCode) throws DaoException {
		try (final Connection connection = connectionPool.takeConnection();
			 final PreparedStatement statement = connection.prepareStatement(hasVinCodeQuery)) {
			statement.setInt(1, userId);
			statement.setString(2, vinCode);
			statement.executeQuery();
			return statement.getResultSet().next();
		} catch (SQLException | DBConnectionException e) {
			logger.error(e);
			throw new DaoException(e);
		}
	}

	private Car createCarByResultSet(ResultSet rs) throws SQLException {
		final Car car = new Car();
		car.setId(rs.getInt(1));
		car.setBrand(rs.getString(2));
		car.setModel(rs.getString(3));
		car.setYear(rs.getInt(4));
		car.setRegNumber(rs.getString(5));
		car.setEngineCapacity(rs.getInt(6));
		car.setVinCode(rs.getString(7));
		car.setUserId(rs.getInt(8));
		return car;
	}

}
