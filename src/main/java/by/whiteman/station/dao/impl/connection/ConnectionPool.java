package by.whiteman.station.dao.impl.connection;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;


public final class ConnectionPool {

	private static final Logger logger = LogManager.getLogger(ConnectionPool.class);

	private static final String DB_CONNECTION_DRIVER = "db.connection.driver";
	private static final String DB_CONNECTION_URL = "db.connection.url";
	private static final String DB_CONNECTION_USER = "db.connection.user";
	private static final String DB_CONNECTION_PASSWORD = "db.connection.password";
	private static final String DB_CONNECTION_POOL_SIZE = "db.connection.poolSize";

	private static volatile ConnectionPool INSTANCE = null;

	private BlockingQueue<Connection> connectionQueue;
	private BlockingQueue<Connection> waitingQueue;

	private final String driver;
	private final String url;
	private final String user;
	private final String password;
	private final int poolSize;

	private ConnectionPool() {
		final DBResourceManager resourceManager = DBResourceManager.getInstance();
		driver = resourceManager.getValue(DB_CONNECTION_DRIVER);
		url = resourceManager.getValue(DB_CONNECTION_URL);
		user = resourceManager.getValue(DB_CONNECTION_USER);
		password = resourceManager.getValue(DB_CONNECTION_PASSWORD);
		poolSize = Integer.parseInt(resourceManager.getValue(DB_CONNECTION_POOL_SIZE));
	}

	public static ConnectionPool getInstance() {
		ConnectionPool connectionPool = INSTANCE;
		if (connectionPool == null) {
			synchronized (ConnectionPool.class) {
				connectionPool = INSTANCE;
				if (connectionPool == null) {
					INSTANCE = connectionPool = new ConnectionPool();
				}
			}
		}
		return connectionPool;
	}

	public void initPoolData(boolean autoCommit) throws DBConnectionException {
		try {
			Class.forName(driver);
			connectionQueue = new ArrayBlockingQueue<>(poolSize);
			waitingQueue = new ArrayBlockingQueue<>(poolSize);
			for (int i = 0; i < poolSize; i++) {
				final Connection connection = DriverManager.getConnection(url, user, password);
				final PooledConnection pooledConnection = new PooledConnection(connection, connectionQueue, waitingQueue);
				pooledConnection.setAutoCommit(autoCommit);
				connectionQueue.add(pooledConnection);
			}
		} catch (ClassNotFoundException | SQLException e) {
			logger.error(e);
			throw new DBConnectionException(e);
		}
	}

	public Connection takeConnection() throws DBConnectionException {
		Connection connection;
		try {
			connection = connectionQueue.take();
			waitingQueue.add(connection);
		} catch (InterruptedException e) {
			logger.error(e);
			throw new DBConnectionException(e);
		}
		return connection;
	}

	public void dispose() throws DBConnectionException {
		try {
			closeConnectionQueue(waitingQueue);
			closeConnectionQueue(connectionQueue);
		} catch (SQLException e) {
			logger.error(e);
			throw new DBConnectionException(e);
		}
	}

	private void closeConnectionQueue(BlockingQueue<Connection> queue) throws SQLException {
		Connection connection;
		while ((connection = queue.poll()) != null) {
			if (!connection.getAutoCommit()) {
				connection.commit();
			}
			((PooledConnection) connection).reallyClose();
		}
	}

	public void rollback() throws SQLException {
		for (Connection connection : connectionQueue) {
			connection.rollback();
		}
	}

}
