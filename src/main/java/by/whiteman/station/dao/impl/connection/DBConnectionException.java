package by.whiteman.station.dao.impl.connection;

public class DBConnectionException extends Exception {

	public DBConnectionException(String message, Throwable cause) {
		super(message, cause);
	}

	public DBConnectionException(Throwable cause) {
		super(cause);
	}

}
