package by.whiteman.station.dao.impl.connection;

import java.util.ResourceBundle;


public final class DBResourceManager {

	private static volatile DBResourceManager INSTANCE = null;
	private final ResourceBundle resourceBundle = ResourceBundle.getBundle("dbConnection");

	private DBResourceManager() {
	}

	public static DBResourceManager getInstance() {
		DBResourceManager manager = INSTANCE;
		if (manager == null) {
			synchronized (DBResourceManager.class) {
				manager = INSTANCE;
				if (manager == null) {
					INSTANCE = manager = new DBResourceManager();
				}
			}
		}
		return manager;
	}

	public String getValue(String key) {
		return resourceBundle.getString(key);
	}

}
