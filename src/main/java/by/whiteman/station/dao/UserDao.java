package by.whiteman.station.dao;

import by.whiteman.station.entity.User;
import by.whiteman.station.entity.UserRole;

import java.util.List;


public interface UserDao {

	Integer createUser(User user) throws DaoException;

	User getUserById(int id) throws DaoException;

	User getUserByEmail(String email) throws DaoException;

	void updateUser(User user) throws DaoException;

	void deleteUser(int id) throws DaoException;

	List<User> getUsersByRole(UserRole role) throws DaoException;

}
