package by.whiteman.station.dao;

import by.whiteman.station.dao.impl.CarDaoImpl;
import by.whiteman.station.dao.impl.OrderDaoImpl;
import by.whiteman.station.dao.impl.UserDaoImpl;


public final class DaoFactory {

	private static volatile DaoFactory INSTANCE = null;

	private final UserDao userDao = new UserDaoImpl();
	private final CarDao carDao = new CarDaoImpl();
	private final OrderDao orderDao = new OrderDaoImpl();

	private DaoFactory() {
	}

	public static DaoFactory getInstance() {
		DaoFactory factory = INSTANCE;
		if (factory == null) {
			synchronized (DaoFactory.class) {
				factory = INSTANCE;
				if (factory == null) {
					INSTANCE = factory = new DaoFactory();
				}
			}
		}
		return factory;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public CarDao getCarDao() {
		return carDao;
	}

	public OrderDao getOrderDao() {
		return orderDao;
	}

}
