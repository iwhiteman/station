package by.whiteman.station.web.constant;

public final class SessionConstant {

	private SessionConstant() {
	}

	public static final String SUCCESS_MESSAGE = "successMessage";
	public static final String ERROR_MESSAGE = "errorMessage";
	public static final String ERROR_MESSAGE_SUPPORT = "errorMessageSupport";
	public static final String USER = "user";
	public static final String LOCALE = "locale";
	public static final String REQUEST_URL = "requestUrl";

}
