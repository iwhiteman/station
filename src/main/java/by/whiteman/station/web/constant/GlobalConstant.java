package by.whiteman.station.web.constant;

public final class GlobalConstant {

	private GlobalConstant() {
	}

	public static final String COMMAND_PARAMETER = "command";

	public static final String NOT_FOUND_PAGE_COMMAND = "NOT_FOUND_PAGE";

	public static final String MAIN_PAGE_COMMAND = "MAIN_PAGE";
	public static final String LOGIN_PAGE_COMMAND = "LOGIN_PAGE";
	public static final String REGISTRATION_PAGE_COMMAND = "REGISTRATION_PAGE";
	public static final String ABOUT_US_PAGE_COMMAND = "ABOUT_US_PAGE";
	public static final String ADD_USER_PAGE_COMMAND = "ADD_USER_PAGE";
	public static final String USER_PROFILE_PAGE_COMMAND = "USER_PROFILE_PAGE";
	public static final String TEAM_MANAGEMENT_PAGE_COMMAND = "TEAM_MANAGEMENT_PAGE";
	public static final String EDIT_USER_PAGE_COMMAND = "EDIT_USER_PAGE";
	public static final String CARS_PAGE_COMMAND = "CARS_PAGE";
	public static final String CAR_INFO_PAGE_COMMAND = "CAR_INFO_PAGE";
	public static final String EDIT_CAR_PAGE_COMMAND = "EDIT_CAR_PAGE";
	public static final String EDIT_ORDER_PAGE_COMMAND = "EDIT_ORDER_PAGE";
	public static final String VIEW_ORDER_PAGE_COMMAND = "VIEW_ORDER_PAGE";
	public static final String ORDERS_MANAGEMENT_PAGE_COMMAND = "ORDERS_MANAGEMENT_PAGE";
	public static final String MY_WORK_PAGE_COMMAND = "MY_WORK_PAGE";

	public static final String LOGOUT_USER_COMMAND = "LOGOUT_USER";
	public static final String LOGIN_USER_COMMAND = "LOGIN_USER";
	public static final String REGISTRATION_USER_COMMAND = "REGISTRATION_USER";
	public static final String CHANGE_LANGUAGE_COMMAND = "CHANGE_LANGUAGE";
	public static final String UPDATE_USER_PROFILE_COMMAND = "UPDATE_USER_PROFILE";
	public static final String DELETE_USER_COMMAND = "DELETE_USER";
	public static final String SAVE_CAR_COMMAND = "SAVE_CAR";
	public static final String SAVE_ORDER_COMMAND = "SAVE_ORDER";

	public static final String PAGE_TITLE_ATTRIBUTE = "pageTitle";
	public static final String PAGE_PATH_ATTRIBUTE = "pagePath";
	public static final String MENU_ITEMS_ATTRIBUTE = "menuItems";
	public static final String ROLES_ATTRIBUTE = "roles";
	public static final String CAR_ATTRIBUTE = "car";
	public static final String CARS_ATTRIBUTE = "cars";
	public static final String ORDER_ATTRIBUTE = "order";
	public static final String ORDERS_ATTRIBUTE = "orders";
	public static final String STATUSES_ATTRIBUTE = "statuses";
	public static final String MASTERS_ATTRIBUTE = "masters";
	public static final String MANAGERS_ATTRIBUTE = "managers";
	public static final String EDIT_USER_ATTRIBUTE = "editUser";
	public static final String LOGIN_PAGE_ATTRIBUTE = "loginPage";
	public static final String ERROR_PAGE_ATTRIBUTE = "errorPage";
	public static final String REGISTRATION_PAGE_ATTRIBUTE = "registrationPage";
	public static final String NEW_ORDERS_ATTRIBUTE = "newOrders";
	public static final String ORDERS_IN_WORK_ATTRIBUTE = "ordersInWork";
	public static final String HAS_ORDERS_IN_WORK_ATTRIBUTE = "hasOrdersInWork";
	public static final String ORDER_VO_ATTRIBUTE = "orderVO";

	public static final String USER_ID_PARAMETER = "userId";
	public static final String PASSWORD_PARAMETER = "password";
	public static final String EMAIL_PARAMETER = "email";
	public static final String FIRST_NAME_PARAMETER = "firstName";
	public static final String LAST_NAME_PARAMETER = "lastName";
	public static final String ROLE_ID_PARAMETER = "roleId";
	public static final String OWNER_HASH_PARAMETER = "ownerHash";
	public static final String BRAND_PARAMETER = "brand";
	public static final String MODEL_PARAMETER = "model";
	public static final String YEAR_PARAMETER = "year";
	public static final String ENGINE_CAPACITY_PARAMETER = "engineCapacity";
	public static final String VIN_CODE_PARAMETER = "vinCode";
	public static final String REG_NUMBER_PARAMETER = "regNumber";
	public static final String CAR_ID_PARAMETER = "carId";
	public static final String CAR_MILEAGE_PARAMETER = "carMileage";
	public static final String DATE_PARAMETER = "date";
	public static final String DESCRIPTION_PARAMETER = "description";
	public static final String ORDER_ID_PARAMETER = "orderId";
	public static final String PERSONAL_DATA_PARAMETER = "personalData";
	public static final String PHONE_NUMBER_PARAMETER = "phoneNumber";
	public static final String OLD_PASSWORD_PARAMETER = "oldPassword";
	public static final String NEW_PASSWORD_PARAMETER = "newPassword";
	public static final String PRICE_PARAMETER = "price";
	public static final String ORDER_STATUS_PARAMETER = "orderStatus";
	public static final String END_DATE_PARAMETER = "endDate";
	public static final String MASTER_NOTES_PARAMETER = "masterNotes";
	public static final String MASTER_ID_PARAMETER = "masterId";

}
