package by.whiteman.station.web.filter;

import by.whiteman.station.util.CommonUtil;
import by.whiteman.station.web.constant.SessionConstant;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class LocaleFilter implements Filter {

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		String locale = servletRequest.getParameter(SessionConstant.LOCALE);
		final HttpSession session = ((HttpServletRequest) servletRequest).getSession();
		if (locale == null && session.getAttribute(SessionConstant.LOCALE) == null) {
			locale = servletRequest.getLocale().getLanguage();
		}
		if (!CommonUtil.isEmptyString(locale)) {
			session.setAttribute(SessionConstant.LOCALE, locale);
		}

		filterChain.doFilter(servletRequest, servletResponse);
	}

}
