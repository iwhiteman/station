package by.whiteman.station.web.filter;

import by.whiteman.station.util.SessionUtil;
import by.whiteman.station.web.command.Command;
import by.whiteman.station.web.constant.GlobalConstant;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class AuthFilter implements Filter {

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		final HttpServletRequest request = (HttpServletRequest) servletRequest;
		final HttpServletResponse response = (HttpServletResponse) servletResponse;

		final String commandName = request.getParameter(GlobalConstant.COMMAND_PARAMETER);

		if (SessionUtil.getUser(request) == null) {
			if (!GlobalConstant.MAIN_PAGE_COMMAND.equals(commandName)
					&& !GlobalConstant.CHANGE_LANGUAGE_COMMAND.equals(commandName)
					&& !GlobalConstant.LOGIN_PAGE_COMMAND.equals(commandName)
					&& !GlobalConstant.REGISTRATION_PAGE_COMMAND.equals(commandName)
					&& !GlobalConstant.LOGIN_USER_COMMAND.equals(commandName)
					&& !GlobalConstant.REGISTRATION_USER_COMMAND.equals(commandName)) {
				response.sendRedirect(request.getContextPath() + Command.DEFAULT_URL + GlobalConstant.LOGIN_PAGE_COMMAND);
				return;
			}
		} else {
			if (GlobalConstant.LOGIN_PAGE_COMMAND.equals(commandName) || GlobalConstant.REGISTRATION_PAGE_COMMAND.equals(commandName)) {
				response.sendRedirect(request.getContextPath() + Command.DEFAULT_URL + GlobalConstant.MAIN_PAGE_COMMAND);
				return;
			}
		}

		filterChain.doFilter(servletRequest, servletResponse);
	}

}
