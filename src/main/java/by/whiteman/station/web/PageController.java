package by.whiteman.station.web;

import by.whiteman.station.dao.impl.connection.ConnectionPool;
import by.whiteman.station.dao.impl.connection.DBConnectionException;
import by.whiteman.station.entity.User;
import by.whiteman.station.entity.UserRole;
import by.whiteman.station.util.SessionUtil;
import by.whiteman.station.web.command.ApplicationPage;
import by.whiteman.station.web.command.Command;
import by.whiteman.station.web.constant.GlobalConstant;
import by.whiteman.station.web.constant.SessionConstant;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public final class PageController extends HttpServlet {

	private final CommandProvider provider = new CommandProvider();
	private final MainMenuProvider menuProvider = new MainMenuProvider();

	@Override
	public void init() throws ServletException {
		try {
			ConnectionPool.getInstance().initPoolData(true);
		} catch (DBConnectionException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void destroy() {
		try {
			ConnectionPool.getInstance().dispose();
		} catch (DBConnectionException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		process(request, response);
	}

	private void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		final String commandName = request.getParameter(GlobalConstant.COMMAND_PARAMETER);
		final Command command = provider.getCommand(commandName);

		if (command instanceof ApplicationPage) {
			final String requestURL = request.getRequestURI() + "?" + request.getQueryString();
			request.getSession().setAttribute(SessionConstant.REQUEST_URL, requestURL);

			final ApplicationPage pageCommand = (ApplicationPage) command;
			request.setAttribute(GlobalConstant.PAGE_TITLE_ATTRIBUTE, pageCommand.getPageTitle());
			request.setAttribute(GlobalConstant.PAGE_PATH_ATTRIBUTE, pageCommand.getPagePath());
			final User user = SessionUtil.getUser(request);
			final UserRole userRole = user == null ? null : user.getUserRole();
			request.setAttribute(GlobalConstant.MENU_ITEMS_ATTRIBUTE, menuProvider.getMenuItems(userRole, commandName));
		}

		command.execute(request, response);
	}

}
