package by.whiteman.station.web.command;

public interface ApplicationPage {

	/**
	 * Заголовок (title) страницы
	 */
	String getPageTitle();

	/**
	 * Относительный путь к содержимому страницы (от пакета /WEB-INF/page)
	 */
	String getPagePath();

}
