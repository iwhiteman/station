package by.whiteman.station.web.command.impl;

import by.whiteman.station.entity.User;
import by.whiteman.station.entity.UserRole;
import by.whiteman.station.service.ServiceException;
import by.whiteman.station.service.ServiceFactory;
import by.whiteman.station.service.UserService;
import by.whiteman.station.util.BCryptEncoder;
import by.whiteman.station.util.CommonUtil;
import by.whiteman.station.util.ServletUtil;
import by.whiteman.station.web.command.Command;
import by.whiteman.station.web.constant.GlobalConstant;
import by.whiteman.station.web.constant.SessionConstant;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class RegistrationUserCommand implements Command {

	private static final UserService userService = ServiceFactory.getInstance().getUserService();

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		final String firstName = request.getParameter(GlobalConstant.FIRST_NAME_PARAMETER);
		final String lastName = request.getParameter(GlobalConstant.LAST_NAME_PARAMETER);
		final String roleId = request.getParameter(GlobalConstant.ROLE_ID_PARAMETER);
		final String email = request.getParameter(GlobalConstant.EMAIL_PARAMETER);
		final String password = request.getParameter(GlobalConstant.PASSWORD_PARAMETER);
		final String ownerHash = request.getParameter(GlobalConstant.OWNER_HASH_PARAMETER);

		final boolean registrationPage = roleId == null;

		if (CommonUtil.isEmptyString(email) || CommonUtil.isEmptyString(firstName) || CommonUtil.isEmptyString(lastName) || CommonUtil.isEmptyString(password)) {
			request.getSession().setAttribute(SessionConstant.ERROR_MESSAGE, "page.registration.error.empty.fields");
			ServletUtil.sendRedirect(request, response, getRedirectCommandName(registrationPage));
		} else {
			try {
				final User userByEmail = userService.getUserByEmail(email);
				if (userByEmail != null) {
					request.getSession().setAttribute(SessionConstant.ERROR_MESSAGE, "page.registration.error.user.exist");
					ServletUtil.sendRedirect(request, response, getRedirectCommandName(registrationPage));
				} else {
					final User user = new User();
					user.setFirstName(firstName);
					user.setLastName(lastName);
					user.setEmail(email);
					user.setPasswordHash(BCryptEncoder.getPasswordHash(password));

					if (CommonUtil.isEmptyString(ownerHash)) {
						final UserRole userRole = registrationPage ? UserRole.USER : UserRole.valueOf(Integer.parseInt(roleId));
						user.setUserRole(userRole);
						createUser(user, request, response, registrationPage);
					} else {
						final List<User> usersByRole = userService.getUsersByRole(UserRole.OWNER);
						if (usersByRole.isEmpty()) {
							user.setUserRole(UserRole.OWNER);
							createUser(user, request, response, registrationPage);
						} else {
							request.getSession().setAttribute(SessionConstant.ERROR_MESSAGE, "page.registration.error.has.owner");
							ServletUtil.sendRedirect(request, response, getRedirectCommandName(registrationPage));
						}
					}
				}
			} catch (ServiceException e) {
				request.getSession().setAttribute(SessionConstant.ERROR_MESSAGE, "page.registration.error.ups");
				ServletUtil.sendRedirect(request, response, getRedirectCommandName(registrationPage));
			}
		}
	}

	private void createUser(User user, HttpServletRequest request, HttpServletResponse response, boolean registrationPage) throws ServletException, IOException, ServiceException {
		userService.createUser(user);
		if (registrationPage) {
			request.getSession().setAttribute(SessionConstant.USER, user);
			ServletUtil.sendRedirect(request, response, GlobalConstant.MAIN_PAGE_COMMAND);
		} else {
			request.getSession().setAttribute(SessionConstant.SUCCESS_MESSAGE, "page.registration.success.user.added");
			ServletUtil.sendRedirect(request, response, GlobalConstant.ADD_USER_PAGE_COMMAND);
		}
	}

	private String getRedirectCommandName(boolean registrationPage) {
		return registrationPage ? GlobalConstant.REGISTRATION_PAGE_COMMAND : GlobalConstant.ADD_USER_PAGE_COMMAND;
	}

}
