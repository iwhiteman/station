package by.whiteman.station.web.command.impl.page;

import by.whiteman.station.entity.UserRole;
import by.whiteman.station.service.ServiceException;
import by.whiteman.station.service.ServiceFactory;
import by.whiteman.station.service.UserService;
import by.whiteman.station.util.ServletUtil;
import by.whiteman.station.web.command.ApplicationPage;
import by.whiteman.station.web.command.Command;
import by.whiteman.station.web.constant.GlobalConstant;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class TeamManagementPageCommand implements ApplicationPage, Command {

	private static final UserService userService = ServiceFactory.getInstance().getUserService();

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			request.setAttribute(GlobalConstant.MANAGERS_ATTRIBUTE, userService.getUsersByRole(UserRole.MANAGER));
			request.setAttribute(GlobalConstant.MASTERS_ATTRIBUTE, userService.getUsersByRole(UserRole.MASTER));
			ServletUtil.forward(request, response);
		} catch (ServiceException e) {
			ServletUtil.sendRedirect(request, response, GlobalConstant.NOT_FOUND_PAGE_COMMAND);
		}
	}

	@Override
	public String getPageTitle() {
		return "page.team.management.title";
	}

	@Override
	public String getPagePath() {
		return "teamManagementPage.jsp";
	}

}
