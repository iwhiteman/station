package by.whiteman.station.web.command.impl.page;

import by.whiteman.station.entity.OrderVO;
import by.whiteman.station.service.CarService;
import by.whiteman.station.service.OrderService;
import by.whiteman.station.service.ServiceException;
import by.whiteman.station.service.ServiceFactory;
import by.whiteman.station.util.CommonUtil;
import by.whiteman.station.util.ServletUtil;
import by.whiteman.station.web.command.ApplicationPage;
import by.whiteman.station.web.command.Command;
import by.whiteman.station.web.constant.GlobalConstant;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ViewOrderPageCommand implements ApplicationPage, Command {

	private static final CarService carService = ServiceFactory.getInstance().getCarService();
	private static final OrderService orderService = ServiceFactory.getInstance().getOrderService();

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String orderId = request.getParameter(GlobalConstant.ORDER_ID_PARAMETER);

		if (CommonUtil.isEmptyString(orderId)) {
			ServletUtil.sendRedirect(request, response, GlobalConstant.NOT_FOUND_PAGE_COMMAND);
		} else {
			try {
				final OrderVO orderVO = new OrderVO();
				orderVO.setOrder(orderService.getOrder(Integer.parseInt(orderId)));
				orderVO.setOrderHistory(orderService.getOrderHistory(Integer.parseInt(orderId)));
				orderVO.setCar(carService.getCar(orderVO.getOrder().getCarId()));
				request.setAttribute(GlobalConstant.ORDER_VO_ATTRIBUTE, orderVO);
				ServletUtil.forward(request, response);
			} catch (ServiceException e) {
				ServletUtil.sendRedirect(request, response, GlobalConstant.NOT_FOUND_PAGE_COMMAND);
			}
		}
	}

	@Override
	public String getPageTitle() {
		return "page.edit.order.title";
	}

	@Override
	public String getPagePath() {
		return "viewOrderPage.jsp";
	}

}
