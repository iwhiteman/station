package by.whiteman.station.web.command.impl;

import by.whiteman.station.entity.User;
import by.whiteman.station.service.ServiceException;
import by.whiteman.station.service.ServiceFactory;
import by.whiteman.station.service.UserService;
import by.whiteman.station.util.BCryptEncoder;
import by.whiteman.station.util.CommonUtil;
import by.whiteman.station.util.ServletUtil;
import by.whiteman.station.util.SessionUtil;
import by.whiteman.station.web.command.Command;
import by.whiteman.station.web.constant.GlobalConstant;
import by.whiteman.station.web.constant.SessionConstant;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class UpdateUserProfileCommand implements Command {

	private static final UserService userService = ServiceFactory.getInstance().getUserService();

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final User user = SessionUtil.getUser(request);
		final boolean savePersonalData = Boolean.parseBoolean(request.getParameter(GlobalConstant.PERSONAL_DATA_PARAMETER));

		try {
			if (savePersonalData) {
				final String firstName = request.getParameter(GlobalConstant.FIRST_NAME_PARAMETER);
				final String lastName = request.getParameter(GlobalConstant.LAST_NAME_PARAMETER);
				final String phoneNumber = request.getParameter(GlobalConstant.PHONE_NUMBER_PARAMETER);

				if (CommonUtil.isEmptyString(firstName) || CommonUtil.isEmptyString(lastName)) {
					request.getSession().setAttribute(SessionConstant.ERROR_MESSAGE_SUPPORT, "page.user.profile.error.fill.name");
					ServletUtil.sendRedirect(request, response, GlobalConstant.USER_PROFILE_PAGE_COMMAND);
				} else {
					user.setFirstName(firstName);
					user.setLastName(lastName);
					user.setPhoneNumber(phoneNumber);

					userService.updateUser(user);
					ServletUtil.sendRedirect(request, response, GlobalConstant.USER_PROFILE_PAGE_COMMAND);
				}
			} else {
				final String email = request.getParameter(GlobalConstant.EMAIL_PARAMETER);
				final String oldPassword = request.getParameter(GlobalConstant.OLD_PASSWORD_PARAMETER);
				final String newPassword = request.getParameter(GlobalConstant.NEW_PASSWORD_PARAMETER);

				if (CommonUtil.isEmptyString(email)) {
					request.getSession().setAttribute(SessionConstant.ERROR_MESSAGE, "page.user.profile.error.fill.email");
					ServletUtil.sendRedirect(request, response, GlobalConstant.USER_PROFILE_PAGE_COMMAND);
				} else if (!email.equals(user.getEmail()) && userService.getUserByEmail(email) != null) {
					request.getSession().setAttribute(SessionConstant.ERROR_MESSAGE, "page.registration.error.user.exist");
					ServletUtil.sendRedirect(request, response, GlobalConstant.USER_PROFILE_PAGE_COMMAND);
				} else {
					final boolean oldPasswordEmpty = CommonUtil.isEmptyString(oldPassword);
					final boolean newPasswordEmpty = CommonUtil.isEmptyString(newPassword);
					if (oldPasswordEmpty && !newPasswordEmpty || !oldPasswordEmpty && newPasswordEmpty) {
						request.getSession().setAttribute(SessionConstant.ERROR_MESSAGE, "page.user.profile.error.fill.password");
						ServletUtil.sendRedirect(request, response, GlobalConstant.USER_PROFILE_PAGE_COMMAND);
					} else if (!newPasswordEmpty && userService.validateCredentials(user.getEmail(), oldPassword) == null) {
						request.getSession().setAttribute(SessionConstant.ERROR_MESSAGE, "page.user.profile.error.bad.password");
						ServletUtil.sendRedirect(request, response, GlobalConstant.USER_PROFILE_PAGE_COMMAND);
					} else {
						if (!newPasswordEmpty) {
							user.setPasswordHash(BCryptEncoder.getPasswordHash(newPassword));
						}
						user.setEmail(email);

						userService.updateUser(user);
						ServletUtil.sendRedirect(request, response, GlobalConstant.USER_PROFILE_PAGE_COMMAND);
					}
				}
			}
		} catch (ServiceException e) {
			request.getSession().setAttribute(SessionConstant.ERROR_MESSAGE, "page.registration.error.ups");
			ServletUtil.sendRedirect(request, response, GlobalConstant.USER_PROFILE_PAGE_COMMAND);
		}
	}

}
