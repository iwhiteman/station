package by.whiteman.station.web.command.impl.page;

import by.whiteman.station.util.ServletUtil;
import by.whiteman.station.web.command.ApplicationPage;
import by.whiteman.station.web.command.Command;
import by.whiteman.station.web.constant.GlobalConstant;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class RegistrationPageCommand implements ApplicationPage, Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute(GlobalConstant.REGISTRATION_PAGE_ATTRIBUTE, true);
		request.setAttribute(GlobalConstant.OWNER_HASH_PARAMETER, request.getParameter(GlobalConstant.OWNER_HASH_PARAMETER));
		ServletUtil.forward(request, response);
	}

	@Override
	public String getPageTitle() {
		return "page.registration.title";
	}

	@Override
	public String getPagePath() {
		return "registrationPage.jsp";
	}

}
