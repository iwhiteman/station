package by.whiteman.station.web.command.impl.page;

import by.whiteman.station.service.CarService;
import by.whiteman.station.service.ServiceException;
import by.whiteman.station.service.ServiceFactory;
import by.whiteman.station.util.ServletUtil;
import by.whiteman.station.util.SessionUtil;
import by.whiteman.station.web.command.ApplicationPage;
import by.whiteman.station.web.command.Command;
import by.whiteman.station.web.constant.GlobalConstant;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class CarsPageCommand implements ApplicationPage, Command {

	private static final CarService carService = ServiceFactory.getInstance().getCarService();

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			request.setAttribute(GlobalConstant.CARS_ATTRIBUTE, carService.getCars(SessionUtil.getUser(request).getId()));
			ServletUtil.forward(request, response);
		} catch (ServiceException e) {
			ServletUtil.sendRedirect(request, response, GlobalConstant.NOT_FOUND_PAGE_COMMAND);
		}
	}

	@Override
	public String getPageTitle() {
		return "page.cars.title";
	}

	@Override
	public String getPagePath() {
		return "carsPage.jsp";
	}

}
