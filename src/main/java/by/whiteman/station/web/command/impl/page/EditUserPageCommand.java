package by.whiteman.station.web.command.impl.page;

import by.whiteman.station.entity.User;
import by.whiteman.station.service.ServiceException;
import by.whiteman.station.service.ServiceFactory;
import by.whiteman.station.service.UserService;
import by.whiteman.station.util.ServletUtil;
import by.whiteman.station.web.command.ApplicationPage;
import by.whiteman.station.web.command.Command;
import by.whiteman.station.web.constant.GlobalConstant;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class EditUserPageCommand implements ApplicationPage, Command {

	private static final UserService userService = ServiceFactory.getInstance().getUserService();

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String userId = request.getParameter(GlobalConstant.USER_ID_PARAMETER);

		try {
			final User userById = userService.getUserById(Integer.parseInt(userId));
			if (userById != null) {
				request.setAttribute(GlobalConstant.EDIT_USER_ATTRIBUTE, userById);
				ServletUtil.forward(request, response);
			} else {
				ServletUtil.sendRedirect(request, response, GlobalConstant.NOT_FOUND_PAGE_COMMAND);
			}
		} catch (ServiceException e) {
			ServletUtil.sendRedirect(request, response, GlobalConstant.NOT_FOUND_PAGE_COMMAND);
		}
	}

	@Override
	public String getPageTitle() {
		return "page.edit.user.title";
	}

	@Override
	public String getPagePath() {
		return "editUserPage.jsp";
	}

}
