package by.whiteman.station.web.command.impl;

import by.whiteman.station.web.command.Command;
import by.whiteman.station.web.constant.SessionConstant;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ChangeLanguageCommand implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect((String) request.getSession().getAttribute(SessionConstant.REQUEST_URL));
	}

}
