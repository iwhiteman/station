package by.whiteman.station.web.command.impl.page;

import by.whiteman.station.util.ServletUtil;
import by.whiteman.station.web.command.ApplicationPage;
import by.whiteman.station.web.command.Command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class MainPageCommand implements ApplicationPage, Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletUtil.forward(request, response);
	}

	@Override
	public String getPageTitle() {
		return "page.main.title";
	}

	@Override
	public String getPagePath() {
		return "welcomePage.jsp";
	}

}
