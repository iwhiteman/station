package by.whiteman.station.web.command.impl.page;

import by.whiteman.station.entity.Car;
import by.whiteman.station.entity.Order;
import by.whiteman.station.service.CarService;
import by.whiteman.station.service.OrderService;
import by.whiteman.station.service.ServiceException;
import by.whiteman.station.service.ServiceFactory;
import by.whiteman.station.util.CommonUtil;
import by.whiteman.station.util.ServletUtil;
import by.whiteman.station.web.command.ApplicationPage;
import by.whiteman.station.web.command.Command;
import by.whiteman.station.web.constant.GlobalConstant;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class CarInfoPageCommand implements ApplicationPage, Command {

	private static final CarService carService = ServiceFactory.getInstance().getCarService();
	private static final OrderService orderService = ServiceFactory.getInstance().getOrderService();

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String carId = request.getParameter(GlobalConstant.CAR_ID_PARAMETER);

		if (CommonUtil.isEmptyString(carId)) {
			ServletUtil.sendRedirect(request, response, GlobalConstant.NOT_FOUND_PAGE_COMMAND);
		} else {
			try {
				final Car car = carService.getCar(Integer.parseInt(carId));
				final List<Order> orders = orderService.getOrders(Integer.parseInt(carId));
				request.setAttribute(GlobalConstant.CAR_ATTRIBUTE, car);
				request.setAttribute(GlobalConstant.ORDERS_ATTRIBUTE, orders);
				ServletUtil.forward(request, response);
			} catch (ServiceException e) {
				ServletUtil.sendRedirect(request, response, GlobalConstant.NOT_FOUND_PAGE_COMMAND);
			}
		}
	}

	@Override
	public String getPageTitle() {
		return "page.car.info.title";
	}

	@Override
	public String getPagePath() {
		return "carInfoPage.jsp";
	}

}
