package by.whiteman.station.web.command.impl;

import by.whiteman.station.entity.User;
import by.whiteman.station.entity.UserRole;
import by.whiteman.station.service.ServiceException;
import by.whiteman.station.service.ServiceFactory;
import by.whiteman.station.service.UserService;
import by.whiteman.station.util.CommonUtil;
import by.whiteman.station.util.ServletUtil;
import by.whiteman.station.web.command.Command;
import by.whiteman.station.web.constant.GlobalConstant;
import by.whiteman.station.web.constant.SessionConstant;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class LoginUserCommand implements Command {

	private static final UserService userService = ServiceFactory.getInstance().getUserService();

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		final String email = request.getParameter(GlobalConstant.EMAIL_PARAMETER);
		final String password = request.getParameter(GlobalConstant.PASSWORD_PARAMETER);

		if (CommonUtil.isEmptyString(email) || CommonUtil.isEmptyString(password)) {
			request.getSession().setAttribute(SessionConstant.ERROR_MESSAGE, "page.login.error.enter.credentials");
			ServletUtil.sendRedirect(request, response, GlobalConstant.LOGIN_PAGE_COMMAND);
		} else {
			try {
				final User user = userService.validateCredentials(email, password);
				if (user != null) {
					request.getSession().setAttribute(SessionConstant.USER, user);
					ServletUtil.sendRedirect(request, response, getRedirectCommand(user.getUserRole()));
				} else {
					request.getSession().setAttribute(SessionConstant.ERROR_MESSAGE, "page.login.error.no.user");
					ServletUtil.sendRedirect(request, response, GlobalConstant.LOGIN_PAGE_COMMAND);
				}
			} catch (ServiceException e) {
				ServletUtil.sendRedirect(request, response, GlobalConstant.NOT_FOUND_PAGE_COMMAND);
			}
		}
	}

	private String getRedirectCommand(UserRole userRole) {
		switch (userRole) {
			case OWNER:
				return GlobalConstant.TEAM_MANAGEMENT_PAGE_COMMAND;
			case USER:
				return GlobalConstant.CARS_PAGE_COMMAND;
			case MANAGER:
				return GlobalConstant.ORDERS_MANAGEMENT_PAGE_COMMAND;
			case MASTER:
				return GlobalConstant.MY_WORK_PAGE_COMMAND;
			default:
				return GlobalConstant.MAIN_PAGE_COMMAND;
		}
	}

}
