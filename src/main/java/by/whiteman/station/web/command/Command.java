package by.whiteman.station.web.command;

import by.whiteman.station.web.constant.GlobalConstant;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public interface Command {

	String COMMON_LAYOUT = "/WEB-INF/common/layout.jsp";
	String DEFAULT_URL = "/pageController?" + GlobalConstant.COMMAND_PARAMETER + "=";

	void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;

}
