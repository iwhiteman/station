package by.whiteman.station.web.command.impl;

import by.whiteman.station.service.ServiceException;
import by.whiteman.station.service.ServiceFactory;
import by.whiteman.station.service.UserService;
import by.whiteman.station.util.ServletUtil;
import by.whiteman.station.web.command.Command;
import by.whiteman.station.web.constant.GlobalConstant;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class DeleteUserCommand implements Command {

	private static final UserService userService = ServiceFactory.getInstance().getUserService();

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String userId = request.getParameter(GlobalConstant.USER_ID_PARAMETER);

		try {
			userService.deleteUser(Integer.parseInt(userId));
			ServletUtil.sendRedirect(request, response, GlobalConstant.TEAM_MANAGEMENT_PAGE_COMMAND);
		} catch (ServiceException e) {
			ServletUtil.sendRedirect(request, response, GlobalConstant.NOT_FOUND_PAGE_COMMAND);
		}
	}
}
