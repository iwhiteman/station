package by.whiteman.station.web.command.impl.page;

import by.whiteman.station.entity.UserRole;
import by.whiteman.station.util.ServletUtil;
import by.whiteman.station.web.command.ApplicationPage;
import by.whiteman.station.web.command.Command;
import by.whiteman.station.web.constant.GlobalConstant;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class AddUserPageCommand implements ApplicationPage, Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute(GlobalConstant.ROLES_ATTRIBUTE, Arrays.asList(UserRole.USER, UserRole.MASTER, UserRole.MANAGER));
		ServletUtil.forward(request, response);
	}

	@Override
	public String getPageTitle() {
		return "page.add.user.title";
	}

	@Override
	public String getPagePath() {
		return "addUserPage.jsp";
	}

}
