package by.whiteman.station.web.command.impl.page;

import by.whiteman.station.entity.Car;
import by.whiteman.station.entity.Order;
import by.whiteman.station.entity.OrderStatus;
import by.whiteman.station.entity.UserRole;
import by.whiteman.station.service.CarService;
import by.whiteman.station.service.OrderService;
import by.whiteman.station.service.ServiceException;
import by.whiteman.station.service.ServiceFactory;
import by.whiteman.station.service.UserService;
import by.whiteman.station.util.CommonUtil;
import by.whiteman.station.util.ServletUtil;
import by.whiteman.station.web.command.ApplicationPage;
import by.whiteman.station.web.command.Command;
import by.whiteman.station.web.constant.GlobalConstant;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class EditOrderPageCommand implements ApplicationPage, Command {

	private static final UserService userService = ServiceFactory.getInstance().getUserService();
	private static final CarService carService = ServiceFactory.getInstance().getCarService();
	private static final OrderService orderService = ServiceFactory.getInstance().getOrderService();

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String carId = request.getParameter(GlobalConstant.CAR_ID_PARAMETER);
		final String orderId = request.getParameter(GlobalConstant.ORDER_ID_PARAMETER);

		try {
			if (!CommonUtil.isEmptyString(orderId)) {
				final Order order = orderService.getOrder(Integer.parseInt(orderId));
				final Car car = carService.getCar(order.getCarId());
				request.setAttribute(GlobalConstant.ORDER_ATTRIBUTE, order);
				request.setAttribute(GlobalConstant.CAR_ATTRIBUTE, car);
				request.setAttribute(GlobalConstant.STATUSES_ATTRIBUTE, OrderStatus.values());
				request.setAttribute(GlobalConstant.MASTERS_ATTRIBUTE, userService.getUsersByRole(UserRole.MASTER));
				ServletUtil.forward(request, response);
			} else if (!CommonUtil.isEmptyString(carId)) {
				final Car car = carService.getCar(Integer.parseInt(carId));
				request.setAttribute(GlobalConstant.CAR_ATTRIBUTE, car);
				ServletUtil.forward(request, response);
			} else {
				ServletUtil.sendRedirect(request, response, GlobalConstant.NOT_FOUND_PAGE_COMMAND);
			}
		} catch (ServiceException e) {
			ServletUtil.sendRedirect(request, response, GlobalConstant.NOT_FOUND_PAGE_COMMAND);
		}
	}

	@Override
	public String getPageTitle() {
		return "page.edit.order.title";
	}

	@Override
	public String getPagePath() {
		return "editOrderPage.jsp";
	}

}
