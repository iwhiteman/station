package by.whiteman.station.web.command.impl;

import by.whiteman.station.entity.Order;
import by.whiteman.station.entity.OrderStatus;
import by.whiteman.station.service.OrderService;
import by.whiteman.station.service.ServiceException;
import by.whiteman.station.service.ServiceFactory;
import by.whiteman.station.util.CommonUtil;
import by.whiteman.station.util.ServletUtil;
import by.whiteman.station.util.SessionUtil;
import by.whiteman.station.web.command.Command;
import by.whiteman.station.web.constant.GlobalConstant;
import by.whiteman.station.web.constant.SessionConstant;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class SaveOrderCommand implements Command {

	private static final OrderService orderService = ServiceFactory.getInstance().getOrderService();

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final Integer userId = SessionUtil.getUser(request).getId();

		final String carMileage = request.getParameter(GlobalConstant.CAR_MILEAGE_PARAMETER);
		final String date = request.getParameter(GlobalConstant.DATE_PARAMETER);
		final String description = request.getParameter(GlobalConstant.DESCRIPTION_PARAMETER);
		final String carId = request.getParameter(GlobalConstant.CAR_ID_PARAMETER);
		final String orderId = request.getParameter(GlobalConstant.ORDER_ID_PARAMETER);
		final boolean createNewOrder = CommonUtil.isEmptyString(orderId);

		final String price = request.getParameter(GlobalConstant.PRICE_PARAMETER);
		final String orderStatus = request.getParameter(GlobalConstant.ORDER_STATUS_PARAMETER);
		final String endDate = request.getParameter(GlobalConstant.END_DATE_PARAMETER);
		final String masterNotes = request.getParameter(GlobalConstant.MASTER_NOTES_PARAMETER);
		final String masterId = request.getParameter(GlobalConstant.MASTER_ID_PARAMETER);

		try {
			if (CommonUtil.isEmptyString(carMileage) || CommonUtil.isEmptyString(date) || CommonUtil.isEmptyString(description)) {
				request.getSession().setAttribute(SessionConstant.ERROR_MESSAGE, "page.registration.error.empty.fields");
				ServletUtil.sendRedirect(request, response, GlobalConstant.EDIT_ORDER_PAGE_COMMAND, createNewOrder ? "&carId=" + carId : "&orderId=" + orderId);
			} else {
				if (createNewOrder) {
					final Order order = new Order();
					order.setStatus(OrderStatus.OPENED);
					order.setCreateDate(LocalDateTime.now());
					order.setStartDate(LocalDateTime.parse(date));
					order.setDescription(description);
					order.setCarId(Integer.parseInt(carId));
					order.setCarMileage(Long.parseLong(carMileage));

					orderService.createOrder(order, userId);
					ServletUtil.sendRedirect(request, response, GlobalConstant.CAR_INFO_PAGE_COMMAND, "&carId=" + carId);
				} else {
					final Order order = orderService.getOrder(Integer.parseInt(orderId));
					final OrderStatus prevOrderStatus = order.getStatus();
					order.setStatus(OrderStatus.valueOf(Integer.parseInt(orderStatus)));
					order.setStartDate(LocalDateTime.parse(date));
					if (!CommonUtil.isEmptyString(endDate)) {
						order.setEndDate(LocalDateTime.parse(endDate));
					}
					order.setDescription(description);
					order.setMasterNotes(masterNotes);
					if (!CommonUtil.isEmptyString(price)) {
						order.setPrice(Integer.parseInt(price));
					}
					order.setCarMileage(Long.parseLong(carMileage));
					order.setMasterId(Integer.parseInt(masterId));
					order.setManagerId(userId);

					orderService.updateOrder(order, userId, !prevOrderStatus.equals(order.getStatus()));
					ServletUtil.sendRedirect(request, response, GlobalConstant.ORDERS_MANAGEMENT_PAGE_COMMAND);
				}
			}
		} catch (ServiceException e) {
			request.getSession().setAttribute(SessionConstant.ERROR_MESSAGE, "page.registration.error.ups");
			ServletUtil.sendRedirect(request, response, GlobalConstant.EDIT_ORDER_PAGE_COMMAND, createNewOrder ? "&carId=" + carId : "&orderId=" + orderId);
		}
	}

}
