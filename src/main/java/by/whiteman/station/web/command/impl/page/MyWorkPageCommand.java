package by.whiteman.station.web.command.impl.page;

import by.whiteman.station.entity.Order;
import by.whiteman.station.entity.OrderStatus;
import by.whiteman.station.entity.OrderVO;
import by.whiteman.station.service.CarService;
import by.whiteman.station.service.OrderService;
import by.whiteman.station.service.ServiceException;
import by.whiteman.station.service.ServiceFactory;
import by.whiteman.station.util.ServletUtil;
import by.whiteman.station.web.command.ApplicationPage;
import by.whiteman.station.web.command.Command;
import by.whiteman.station.web.constant.GlobalConstant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class MyWorkPageCommand implements ApplicationPage, Command {

	private static final CarService carService = ServiceFactory.getInstance().getCarService();
	private static final OrderService orderService = ServiceFactory.getInstance().getOrderService();

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			final List<OrderVO> ordersInWork = createOrdersList(orderService.getOrdersInWork());
			request.setAttribute(GlobalConstant.ORDERS_IN_WORK_ATTRIBUTE, createOrdersInWorkMap(ordersInWork));
			request.setAttribute(GlobalConstant.HAS_ORDERS_IN_WORK_ATTRIBUTE, !ordersInWork.isEmpty());
			ServletUtil.forward(request, response);
		} catch (ServiceException e) {
			ServletUtil.sendRedirect(request, response, GlobalConstant.NOT_FOUND_PAGE_COMMAND);
		}
	}

	private List<OrderVO> createOrdersList(List<Order> orders) throws ServiceException {
		final List<OrderVO> newOrders = new ArrayList<>();
		for (Order order : orders) {
			final OrderVO orderVO = new OrderVO();
			orderVO.setOrder(order);
			orderVO.setCar(carService.getCar(order.getCarId()));
			newOrders.add(orderVO);
		}
		return newOrders;
	}

	private HashMap<OrderStatus, List<OrderVO>> createOrdersInWorkMap(List<OrderVO> ordersInWork) {
		final HashMap<OrderStatus, List<OrderVO>> result = new LinkedHashMap<>();
		for (OrderStatus status : OrderStatus.values()) {
			if(!OrderStatus.OPENED.equals(status) && !OrderStatus.CLOSED.equals(status) && !OrderStatus.CANCELED.equals(status)) {
				final List<OrderVO> list = ordersInWork.stream().filter(orderVO -> status.equals(orderVO.getOrder().getStatus())).collect(Collectors.toList());
				result.put(status, list);
			}
		}
		return result;
	}

	@Override
	public String getPageTitle() {
		return "page.my.work.title";
	}

	@Override
	public String getPagePath() {
		return "myWorkPage.jsp";
	}

}
