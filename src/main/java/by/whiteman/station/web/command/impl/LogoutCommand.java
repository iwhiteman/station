package by.whiteman.station.web.command.impl;

import by.whiteman.station.util.ServletUtil;
import by.whiteman.station.web.command.Command;
import by.whiteman.station.web.constant.GlobalConstant;
import by.whiteman.station.web.constant.SessionConstant;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class LogoutCommand implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final HttpSession session = request.getSession();
		final String locale = (String) session.getAttribute(SessionConstant.LOCALE);
		session.invalidate();
		request.getSession().setAttribute(SessionConstant.LOCALE, locale);
		ServletUtil.sendRedirect(request, response, GlobalConstant.MAIN_PAGE_COMMAND);
	}
}
