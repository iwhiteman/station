package by.whiteman.station.web.command.impl;

import by.whiteman.station.entity.Car;
import by.whiteman.station.entity.User;
import by.whiteman.station.service.CarService;
import by.whiteman.station.service.ServiceException;
import by.whiteman.station.service.ServiceFactory;
import by.whiteman.station.util.CommonUtil;
import by.whiteman.station.util.ServletUtil;
import by.whiteman.station.util.SessionUtil;
import by.whiteman.station.web.command.Command;
import by.whiteman.station.web.constant.GlobalConstant;
import by.whiteman.station.web.constant.SessionConstant;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class SaveCarCommand implements Command {

	private static final CarService carService = ServiceFactory.getInstance().getCarService();

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		final String brand = request.getParameter(GlobalConstant.BRAND_PARAMETER);
		final String model = request.getParameter(GlobalConstant.MODEL_PARAMETER);
		final String year = request.getParameter(GlobalConstant.YEAR_PARAMETER);
		final String engineCapacity = request.getParameter(GlobalConstant.ENGINE_CAPACITY_PARAMETER);
		final String vinCode = request.getParameter(GlobalConstant.VIN_CODE_PARAMETER);
		final String regNumber = request.getParameter(GlobalConstant.REG_NUMBER_PARAMETER);
		final String carId = request.getParameter(GlobalConstant.CAR_ID_PARAMETER);

		if (CommonUtil.isEmptyString(brand)
				|| CommonUtil.isEmptyString(model)
				|| CommonUtil.isEmptyString(year)
				|| CommonUtil.isEmptyString(engineCapacity)
				|| CommonUtil.isEmptyString(vinCode)
				|| CommonUtil.isEmptyString(regNumber)) {
			request.getSession().setAttribute(SessionConstant.ERROR_MESSAGE, "page.registration.error.empty.fields");
			ServletUtil.sendRedirect(request, response, GlobalConstant.EDIT_CAR_PAGE_COMMAND);
		} else {
			final User user = SessionUtil.getUser(request);
			final boolean createNewCar = CommonUtil.isEmptyString(carId);
			try {
				if (createNewCar) {
					final boolean hasCar = carService.hasVinCode(user.getId(), vinCode);
					if (hasCar) {
						request.getSession().setAttribute(SessionConstant.ERROR_MESSAGE, "page.edit.car.error.car.exist");
						ServletUtil.sendRedirect(request, response, GlobalConstant.EDIT_CAR_PAGE_COMMAND);
					} else {
						final Car car = new Car();
						car.setUserId(user.getId());
						updateCarParams(car, brand, model, year, regNumber, engineCapacity, vinCode);
						carService.createCar(car);
						ServletUtil.sendRedirect(request, response, GlobalConstant.CARS_PAGE_COMMAND);
					}
				} else {
					final Car car = carService.getCar(Integer.parseInt(carId));
					updateCarParams(car, brand, model, year, regNumber, engineCapacity, vinCode);
					carService.updateCar(car);
					ServletUtil.sendRedirect(request, response, GlobalConstant.CAR_INFO_PAGE_COMMAND, "&carId=" + carId);
				}
			} catch (ServiceException e) {
				request.getSession().setAttribute(SessionConstant.ERROR_MESSAGE, "page.registration.error.ups");
				ServletUtil.sendRedirect(request, response, GlobalConstant.EDIT_CAR_PAGE_COMMAND, createNewCar ? "" : "&carId=" + carId);
			}
		}
	}

	private void updateCarParams(Car car, String brand, String model, String year, String regNumber, String engineCapacity, String vinCode) {
		car.setBrand(brand);
		car.setModel(model);
		car.setYear(Integer.parseInt(year));
		car.setRegNumber(regNumber);
		car.setEngineCapacity(Integer.parseInt(engineCapacity));
		car.setVinCode(vinCode);
	}

}
