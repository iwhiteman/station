package by.whiteman.station.web;

import by.whiteman.station.web.command.Command;
import by.whiteman.station.web.command.impl.ChangeLanguageCommand;
import by.whiteman.station.web.command.impl.DeleteUserCommand;
import by.whiteman.station.web.command.impl.LoginUserCommand;
import by.whiteman.station.web.command.impl.LogoutCommand;
import by.whiteman.station.web.command.impl.RegistrationUserCommand;
import by.whiteman.station.web.command.impl.SaveCarCommand;
import by.whiteman.station.web.command.impl.SaveOrderCommand;
import by.whiteman.station.web.command.impl.UpdateUserProfileCommand;
import by.whiteman.station.web.command.impl.page.AddUserPageCommand;
import by.whiteman.station.web.command.impl.page.CarInfoPageCommand;
import by.whiteman.station.web.command.impl.page.CarsPageCommand;
import by.whiteman.station.web.command.impl.page.EditCarPageCommand;
import by.whiteman.station.web.command.impl.page.EditOrderPageCommand;
import by.whiteman.station.web.command.impl.page.EditUserPageCommand;
import by.whiteman.station.web.command.impl.page.LoginPageCommand;
import by.whiteman.station.web.command.impl.page.MainPageCommand;
import by.whiteman.station.web.command.impl.page.MyWorkPageCommand;
import by.whiteman.station.web.command.impl.page.NotFoundPageCommand;
import by.whiteman.station.web.command.impl.page.OrdersManagementPageCommand;
import by.whiteman.station.web.command.impl.page.RegistrationPageCommand;
import by.whiteman.station.web.command.impl.page.TeamManagementPageCommand;
import by.whiteman.station.web.command.impl.page.UserProfilePageCommand;
import by.whiteman.station.web.command.impl.page.ViewOrderPageCommand;
import by.whiteman.station.web.constant.GlobalConstant;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


public final class CommandProvider {

	private final Map<String, Command> commands = new HashMap<>();

	public CommandProvider() {
		commands.put(GlobalConstant.NOT_FOUND_PAGE_COMMAND, new NotFoundPageCommand());

		commands.put(GlobalConstant.MAIN_PAGE_COMMAND, new MainPageCommand());
		commands.put(GlobalConstant.LOGIN_PAGE_COMMAND, new LoginPageCommand());
		commands.put(GlobalConstant.REGISTRATION_PAGE_COMMAND, new RegistrationPageCommand());
		commands.put(GlobalConstant.USER_PROFILE_PAGE_COMMAND, new UserProfilePageCommand());
		commands.put(GlobalConstant.ADD_USER_PAGE_COMMAND, new AddUserPageCommand());
		commands.put(GlobalConstant.TEAM_MANAGEMENT_PAGE_COMMAND, new TeamManagementPageCommand());
		commands.put(GlobalConstant.EDIT_USER_PAGE_COMMAND, new EditUserPageCommand());
		commands.put(GlobalConstant.CARS_PAGE_COMMAND, new CarsPageCommand());
		commands.put(GlobalConstant.EDIT_CAR_PAGE_COMMAND, new EditCarPageCommand());
		commands.put(GlobalConstant.CAR_INFO_PAGE_COMMAND, new CarInfoPageCommand());
		commands.put(GlobalConstant.EDIT_ORDER_PAGE_COMMAND, new EditOrderPageCommand());
		commands.put(GlobalConstant.VIEW_ORDER_PAGE_COMMAND, new ViewOrderPageCommand());
		commands.put(GlobalConstant.ORDERS_MANAGEMENT_PAGE_COMMAND, new OrdersManagementPageCommand());
		commands.put(GlobalConstant.MY_WORK_PAGE_COMMAND, new MyWorkPageCommand());

		commands.put(GlobalConstant.LOGOUT_USER_COMMAND, new LogoutCommand());
		commands.put(GlobalConstant.LOGIN_USER_COMMAND, new LoginUserCommand());
		commands.put(GlobalConstant.REGISTRATION_USER_COMMAND, new RegistrationUserCommand());
		commands.put(GlobalConstant.CHANGE_LANGUAGE_COMMAND, new ChangeLanguageCommand());
		commands.put(GlobalConstant.UPDATE_USER_PROFILE_COMMAND, new UpdateUserProfileCommand());
		commands.put(GlobalConstant.DELETE_USER_COMMAND, new DeleteUserCommand());
		commands.put(GlobalConstant.SAVE_CAR_COMMAND, new SaveCarCommand());
		commands.put(GlobalConstant.SAVE_ORDER_COMMAND, new SaveOrderCommand());
	}

	public Command getCommand(String commandName) {
		return Optional.ofNullable(commands.get(commandName)).orElseGet(() -> getCommand(GlobalConstant.NOT_FOUND_PAGE_COMMAND));
	}

}
