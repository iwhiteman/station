package by.whiteman.station.web;

import by.whiteman.station.entity.MenuItem;
import by.whiteman.station.entity.UserRole;
import by.whiteman.station.web.constant.GlobalConstant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public final class MainMenuProvider {

	private final Map<UserRole, List<MenuItem>> menuItems = new HashMap<>();

	private static final String MY_TEAM_TITLE = "menu.my.team";
	private static final String USER_PROFILE_TITLE = "menu.user.profile";
	private static final String OUR_ORDERS_TITLE = "menu.our.orders";
	private static final String CARS_TITLE = "menu.cars";
	private static final String ABOUT_US_TITLE = "menu.about.us";
	private static final String MY_WORK_TITLE = "menu.my.work";

	public MainMenuProvider() {
		List<MenuItem> ownerMenuItems = new ArrayList<>();
		ownerMenuItems.add(createMenuItem(MY_TEAM_TITLE, GlobalConstant.TEAM_MANAGEMENT_PAGE_COMMAND));
		ownerMenuItems.add(createMenuItem(USER_PROFILE_TITLE, GlobalConstant.USER_PROFILE_PAGE_COMMAND));
		menuItems.put(UserRole.OWNER, ownerMenuItems);

		List<MenuItem> managerMenuItems = new ArrayList<>();
		managerMenuItems.add(createMenuItem(OUR_ORDERS_TITLE, GlobalConstant.ORDERS_MANAGEMENT_PAGE_COMMAND));
		managerMenuItems.add(createMenuItem(USER_PROFILE_TITLE, GlobalConstant.USER_PROFILE_PAGE_COMMAND));
		menuItems.put(UserRole.MANAGER, managerMenuItems);

		List<MenuItem> masterMenuItems = new ArrayList<>();
		masterMenuItems.add(createMenuItem(MY_WORK_TITLE, GlobalConstant.MY_WORK_PAGE_COMMAND));
		masterMenuItems.add(createMenuItem(USER_PROFILE_TITLE, GlobalConstant.USER_PROFILE_PAGE_COMMAND));
		menuItems.put(UserRole.MASTER, masterMenuItems);

		List<MenuItem> userMenuItems = new ArrayList<>();
		userMenuItems.add(createMenuItem(CARS_TITLE, GlobalConstant.CARS_PAGE_COMMAND));
		userMenuItems.add(createMenuItem(USER_PROFILE_TITLE, GlobalConstant.USER_PROFILE_PAGE_COMMAND));
		menuItems.put(UserRole.USER, userMenuItems);

		List<MenuItem> nonameMenuItems = new ArrayList<>();
		nonameMenuItems.add(createMenuItem(ABOUT_US_TITLE, GlobalConstant.ABOUT_US_PAGE_COMMAND));
		menuItems.put(null, nonameMenuItems);
	}

	private MenuItem createMenuItem(String menuTitle, String commandName) {
		return new MenuItem(menuTitle, commandName);
	}

	public List<MenuItem> getMenuItems(UserRole userRole, String commandName) {
		final List<MenuItem> menuItems = this.menuItems.get(userRole);
		if (menuItems != null) {
			menuItems.forEach(menuItem -> menuItem.setActive(menuItem.getName().equals(commandName)));
			return menuItems;
		} else {
			return Collections.emptyList();
		}
	}
}
