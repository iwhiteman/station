package by.whiteman.station.util;

import by.whiteman.station.entity.User;
import by.whiteman.station.web.constant.SessionConstant;

import javax.servlet.http.HttpServletRequest;


public final class SessionUtil {

	private SessionUtil() {
	}

	public static User getUser(HttpServletRequest request) {
		return (User) request.getSession().getAttribute(SessionConstant.USER);
	}

}
