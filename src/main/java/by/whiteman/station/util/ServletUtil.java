package by.whiteman.station.util;

import by.whiteman.station.web.command.Command;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public final class ServletUtil {

	private ServletUtil() {
	}

	public static void forward(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher(Command.COMMON_LAYOUT);
		dispatcher.forward(request, response);
	}

	public static void sendRedirect(HttpServletRequest request, HttpServletResponse response, String commandName) throws ServletException, IOException {
		sendRedirect(request, response, commandName, "");
	}

	public static void sendRedirect(HttpServletRequest request, HttpServletResponse response, String commandName, String params) throws ServletException, IOException {
		response.sendRedirect(request.getContextPath() + Command.DEFAULT_URL + commandName + params);
	}

}
