package by.whiteman.station.util;

import at.favre.lib.crypto.bcrypt.BCrypt;


public final class BCryptEncoder {

	private static final int COST = 12;

	private BCryptEncoder() {
	}

	public static String getPasswordHash(String password) {
		return BCrypt.withDefaults().hashToString(COST, password.toCharArray());
	}

	public static boolean verifyPassword(String password, String passwordHash) {
		return BCrypt.verifyer().verify(password.toCharArray(), passwordHash).verified;
	}

}
