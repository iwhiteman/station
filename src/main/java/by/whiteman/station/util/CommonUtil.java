package by.whiteman.station.util;

public final class CommonUtil {

	private CommonUtil() {
	}

	public static boolean isEmptyString(String str) {
		return str == null || str.isEmpty();
	}

}
