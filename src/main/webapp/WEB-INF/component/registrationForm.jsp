<%@ page contentType="text/html;charset=UTF-8" %>

<c:if test="${sessionScope.successMessage != null}">
	<div class="alert alert-success" role="alert"><fmt:message bundle="${i18n}" key="${sessionScope.successMessage}"/></div>
	${sessionScope.remove("successMessage")}
</c:if>
<form action="pageController?command=REGISTRATION_USER" method="post">
	<input type="hidden" name="ownerHash" value="${requestScope.ownerHash}">
	<div class="form-floating mb-3">
		<input id="firstNameInput" type="text" name="firstName" class="form-control" placeholder="<fmt:message bundle="${i18n}" key="form.first.name"/>">
		<label for="firstNameInput"><fmt:message bundle="${i18n}" key="form.first.name"/></label>
	</div>
	<div class="form-floating mb-3">
		<input id="lastNameInput" type="text" name="lastName" class="form-control" placeholder="<fmt:message bundle="${i18n}" key="form.last.name"/>">
		<label for="lastNameInput"><fmt:message bundle="${i18n}" key="form.last.name"/></label>
	</div>
	<c:if test="${not empty requestScope.roles}">
		<div class="form-floating mb-3">
			<select id="roleSelect" name="roleId" class="form-select">
				<c:forEach var="role" items="${requestScope.roles}" >
					<option value="${role.id}"><fmt:message bundle="${i18n}" key="${role.name}"/></option>
				</c:forEach>
			</select>
			<label for="roleSelect"><fmt:message bundle="${i18n}" key="form.choose.role"/></label>
		</div>
	</c:if>
	<div class="form-floating mb-3">
		<input id="emailInput" type="email" name="email" class="form-control" placeholder="<fmt:message bundle="${i18n}" key="form.email"/>" autocomplete="new-password">
		<label for="emailInput"><fmt:message bundle="${i18n}" key="form.email"/></label>
	</div>
	<div class="form-floating mb-3">
		<input id="passwordInput" type="password" name="password" class="form-control" placeholder="<fmt:message bundle="${i18n}" key="form.password"/>" autocomplete="new-password">
		<label for="passwordInput"><fmt:message bundle="${i18n}" key="form.password"/></label>
	</div>
	<c:if test="${sessionScope.errorMessage != null}">
		<div class="text-danger mb-3"><fmt:message bundle="${i18n}" key="${sessionScope.errorMessage}"/></div>
		${sessionScope.remove("errorMessage")}
	</c:if>
	<input class="btn btn-primary btn-lg w-100" type="submit" value="<fmt:message bundle="${i18n}" key="form.submit.btn.registration"/>">
</form>
