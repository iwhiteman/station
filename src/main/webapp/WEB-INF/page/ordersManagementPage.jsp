<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="appMessages" var="i18n"/>

<div class="container">
	<div class="d-flex justify-content-between align-items-baseline mt-5 mb-4">
		<h4 class="fw-light"><fmt:message bundle="${i18n}" key="page.orders.caption"/></h4>
<%--		<a class="btn btn-outline-primary" href="pageController?command=EDIT_ORDER_PAGE&carId=${car.id}" role="button"><fmt:message bundle="${i18n}" key="label.create.order"/></a>--%>
	</div>
<%--	<div class="row">--%>
<%--		<div class="col-lg-3">--%>
<%--			<label for="searchCarInput" class="form-label"><fmt:message bundle="${i18n}" key="form.search.car"/></label>--%>
<%--			<input id="searchCarInput" type="text" class="form-control" placeholder="<fmt:message bundle="${i18n}" key="form.reg.number"/>">--%>
<%--		</div>--%>
<%--	</div>--%>
	<h5 class="fw-light mt-4"><fmt:message bundle="${i18n}" key="label.new.orders"/></h5>
	<c:if test="${not empty requestScope.newOrders}">
		<c:forEach var="orderVO" items="${requestScope.newOrders}">
			<c:set var="car" value="${orderVO.car}"/>
			<c:set var="order" value="${orderVO.order}"/>
			<a href="pageController?command=EDIT_ORDER_PAGE&orderId=${order.id}" class="alert alert-primary d-block text-decoration-none w-25" role="alert">
				<span>#${order.id} ${car.brand} ${car.model} ${car.year}</span>
			</a>
		</c:forEach>
	</c:if>
	<c:if test="${empty requestScope.newOrders}">
		<p class="text-secondary"><fmt:message bundle="${i18n}" key="label.no.data"/></p>
	</c:if>
	<h5 class="fw-light mt-5"><fmt:message bundle="${i18n}" key="label.orders.in.work"/></h5>
	<c:if test="${requestScope.hasOrdersInWork}">
		<div class="row border-end">
			<c:forEach var="orderMap" items="${requestScope.ordersInWork}">
				<div class="col-lg-3 border-start">
					<div class="border-bottom py-2 mb-3">${orderMap.key}</div>
					<c:forEach var="orderVO" items="${orderMap.value}">
						<c:set var="car" value="${orderVO.car}"/>
						<c:set var="order" value="${orderVO.order}"/>
						<a href="pageController?command=EDIT_ORDER_PAGE&orderId=${order.id}" class="alert alert-primary d-block text-decoration-none" role="alert">
							<span>#${order.id} ${car.brand} ${car.model} ${car.year}</span>
						</a>
					</c:forEach>
				</div>
			</c:forEach>
		</div>
	</c:if>
	<c:if test="${!requestScope.hasOrdersInWork}">
		<p class="text-secondary"><fmt:message bundle="${i18n}" key="label.no.data"/></p>
	</c:if>
</div>
