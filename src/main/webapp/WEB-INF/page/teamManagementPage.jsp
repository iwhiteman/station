<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="appMessages" var="i18n"/>

<div class="container">
	<div class="d-flex justify-content-between align-items-baseline mt-5 mb-4">
		<h4 class="fw-light"><fmt:message bundle="${i18n}" key="page.team.management.caption"/></h4>
		<a class="btn btn-outline-primary" href="pageController?command=ADD_USER_PAGE" role="button"><fmt:message bundle="${i18n}" key="label.add.user"/></a>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<h5 class="fw-light"><fmt:message bundle="${i18n}" key="label.managers"/></h5>
			<c:set var="hasManagers" value="${not empty requestScope.managers}"/>
			<c:if test="${hasManagers}">
				<table class="table table-sm table-striped table-hover align-middle">
					<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col"><fmt:message bundle="${i18n}" key="form.first.name"/></th>
						<th scope="col"><fmt:message bundle="${i18n}" key="form.phone.number"/></th>
						<th scope="col"></th>
					</tr>
					</thead>
					<tbody>
					<c:forEach var="user" items="${requestScope.managers}" varStatus="counter">
						<tr>
							<td>${counter.count}</td>
							<td>${user.lastName} ${user.firstName}</td>
							<td>${user.phoneNumber}</td>
							<td><a class="btn btn-link" href="pageController?command=EDIT_USER_PAGE&userId=${user.id}" role="button"><fmt:message bundle="${i18n}" key="label.edit"/></a></td>
						</tr>
					</c:forEach>
					</tbody>
				</table>
			</c:if>
			<c:if test="${!hasManagers}">
				<p class="text-secondary"><fmt:message bundle="${i18n}" key="label.no.data"/></p>
			</c:if>
		</div>
		<div class="col-lg-6">
			<h5 class="fw-light"><fmt:message bundle="${i18n}" key="label.masters"/></h5>
			<c:set var="hasMasters" value="${not empty requestScope.masters}"/>
			<c:if test="${hasMasters}">
				<table class="table table-sm table-striped table-hover align-middle">
					<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col"><fmt:message bundle="${i18n}" key="form.first.name"/></th>
						<th scope="col"><fmt:message bundle="${i18n}" key="form.phone.number"/></th>
						<th scope="col"></th>
					</tr>
					</thead>
					<tbody>
					<c:forEach var="user" items="${requestScope.masters}" varStatus="counter">
						<tr>
							<td>${counter.count}</td>
							<td>${user.lastName} ${user.firstName}</td>
							<td>${user.phoneNumber}</td>
							<td><a class="btn btn-link" href="pageController?command=EDIT_USER_PAGE&userId=${user.id}" role="button"><fmt:message bundle="${i18n}" key="label.edit"/></a></td>
						</tr>
					</c:forEach>
					</tbody>
				</table>
			</c:if>
			<c:if test="${!hasMasters}">
				<p class="text-secondary"><fmt:message bundle="${i18n}" key="label.no.data"/></p>
			</c:if>
		</div>
	</div>
</div>
