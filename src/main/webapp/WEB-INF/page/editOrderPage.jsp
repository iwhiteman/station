<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="appMessages" var="i18n"/>

<div class="container mt-5">
	<c:set var="car" value="${requestScope.car}"/>
	<c:set var="order" value="${requestScope.order}"/>
	<c:set var="hasOrder" value="${order != null}"/>
	<nav class="pb-2" aria-label="breadcrumb">
		<ol class="breadcrumb">
			<c:if test="${hasOrder}">
				<li class="breadcrumb-item"><a class="text-decoration-none" href="pageController?command=ORDERS_MANAGEMENT_PAGE" role="button">← <fmt:message bundle="${i18n}" key="label.back"/></a></li>
			</c:if>
			<c:if test="${!hasOrder}">
				<li class="breadcrumb-item"><a class="text-decoration-none" href="pageController?command=CAR_INFO_PAGE&carId=${car.id}" role="button">← <fmt:message bundle="${i18n}" key="label.back"/></a></li>
			</c:if>
		</ol>
	</nav>
	<h4 class="fw-light mb-4"><fmt:message bundle="${i18n}" key="page.edit.order.caption"/> ${car.brand} ${car.model} (${car.regNumber})</h4>
	<div class="row">
		<div class="col-lg-4">
			<p><fmt:message bundle="${i18n}" key="form.year"/>: ${car.year}</p>
			<p><fmt:message bundle="${i18n}" key="form.engine.capacity"/>: ${car.engineCapacity}</p>
			<p><fmt:message bundle="${i18n}" key="form.vin.code"/>: ${car.vinCode}</p>
		</div>
	</div>
	<form action="pageController?command=SAVE_ORDER" method="post">
		<input type="hidden" name="carId" value="${car.id}">
		<input type="hidden" name="orderId" value="${order.id}">
		<div class="row pt-2">
			<div class="col-lg-4">
				<div class="mb-3">
					<label for="carMileageInput" class="form-label"><fmt:message bundle="${i18n}" key="form.mileage"/></label>
					<input id="carMileageInput" type="number" name="carMileage" class="form-control" value="${order.carMileage}">
				</div>
				<div class="mb-3">
					<label for="dateInput" class="form-label"><fmt:message bundle="${i18n}" key="form.date"/></label>
					<input id="dateInput" type="datetime-local" name="date" class="form-control" value="${order.startDate}">
				</div>
				<div class="mb-3">
					<label for="descriptionArea" class="form-label"><fmt:message bundle="${i18n}" key="form.description"/></label>
					<textarea id="descriptionArea" name="description" class="form-control" style="height: 100px">${order.description}</textarea>
				</div>
				<c:if test="${hasOrder}">
					<div class="mb-3">
						<label for="priceInput" class="form-label"><fmt:message bundle="${i18n}" key="form.price"/></label>
						<input id="priceInput" type="number" name="price" class="form-control" value="${order.price}">
					</div>
				</c:if>
				<c:if test="${sessionScope.errorMessage != null}">
					<div class="text-danger mb-3"><fmt:message bundle="${i18n}" key="${sessionScope.errorMessage}"/></div>
					${sessionScope.remove("errorMessage")}
				</c:if>
				<input class="btn btn-primary btn-lg w-100" type="submit" value="<fmt:message bundle="${i18n}" key="form.submit.btn.save"/>">
			</div>
			<c:if test="${hasOrder}">
				<div class="col-lg-4">
					<div class="mb-3">
						<label for="statusSelect" class="form-label"><fmt:message bundle="${i18n}" key="form.order.status"/></label>
						<select id="statusSelect" name="orderStatus" class="form-select">
							<c:forEach var="status" items="${requestScope.statuses}">
								<option value="${status.id}" ${status == order.status ? "selected" : ""}>${status}</option>
							</c:forEach>
						</select>
					</div>
					<div class="mb-3">
						<label for="endDateInput" class="form-label"><fmt:message bundle="${i18n}" key="form.end.date"/></label>
						<input id="endDateInput" type="datetime-local" name="endDate" class="form-control" value="${order.endDate}">
					</div>
					<div class="mb-3">
						<label for="masterNotesArea" class="form-label"><fmt:message bundle="${i18n}" key="form.master.notes"/></label>
						<textarea id="masterNotesArea" name="masterNotes" class="form-control" style="height: 100px">${order.masterNotes}</textarea>
					</div>
					<div class="mb-3">
						<label for="masterSelect" class="form-label"><fmt:message bundle="${i18n}" key="form.choose.master"/></label>
						<select id="masterSelect" name="masterId" class="form-select">
							<option value="0"><fmt:message bundle="${i18n}" key="form.not.selected"/></option>
							<c:forEach var="master" items="${requestScope.masters}">
								<option value="${master.id}" ${master.id == order.masterId ? "selected" : ""}>${master.firstName} ${master.lastName}</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</c:if>
		</div>
	</form>
</div>
