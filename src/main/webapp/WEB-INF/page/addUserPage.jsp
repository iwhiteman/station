<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="appMessages" var="i18n"/>

<div class="container mt-5">
	<nav class="pb-2" aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a class="text-decoration-none" href="pageController?command=TEAM_MANAGEMENT_PAGE" role="button">← <fmt:message bundle="${i18n}" key="label.back"/></a></li>
		</ol>
	</nav>
	<h4 class="fw-light mb-4"><fmt:message bundle="${i18n}" key="page.add.user.caption"/></h4>
	<div class="row">
		<div class="col-lg-4">
			<%@ include file="../component/registrationForm.jsp" %>
		</div>
	</div>
</div>
