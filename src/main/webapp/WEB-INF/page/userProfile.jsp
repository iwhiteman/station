<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="appMessages" var="i18n"/>

<div class="container">
	<h4 class="fw-light mt-5 mb-4"><fmt:message bundle="${i18n}" key="page.user.profile.caption"/></h4>
	<div class="row">
		<div class="col-lg-4">
			<form action="pageController?command=UPDATE_USER_PROFILE" method="post">
				<input type="hidden" name="personalData" value="true">
				<div class="form-floating mb-3">
					<input id="firstNameInput" type="text" name="firstName" class="form-control" placeholder="<fmt:message bundle="${i18n}" key="form.first.name"/>" value="${sessionScope.user.firstName}">
					<label for="firstNameInput"><fmt:message bundle="${i18n}" key="form.first.name"/></label>
				</div>
				<div class="form-floating mb-3">
					<input id="lastNameInput" type="text" name="lastName" class="form-control" placeholder="<fmt:message bundle="${i18n}" key="form.last.name"/>" value="${sessionScope.user.lastName}">
					<label for="lastNameInput"><fmt:message bundle="${i18n}" key="form.last.name"/></label>
				</div>
				<div class="form-floating mb-3">
					<input id="phoneNumberInput" type="tel" name="phoneNumber" class="form-control" placeholder="<fmt:message bundle="${i18n}" key="form.phone.number"/>" value="${sessionScope.user.phoneNumber}">
					<label for="phoneNumberInput"><fmt:message bundle="${i18n}" key="form.phone.number"/></label>
				</div>
				<c:if test="${sessionScope.errorMessageSupport != null}">
					<div class="text-danger mb-3"><fmt:message bundle="${i18n}" key="${sessionScope.errorMessageSupport}"/></div>
					${sessionScope.remove("errorMessageSupport")}
				</c:if>
				<input class="btn btn-primary btn-lg w-100" type="submit" value="<fmt:message bundle="${i18n}" key="form.submit.btn.save"/>">
			</form>
		</div>
		<div class="col-lg-4">
			<form action="pageController?command=UPDATE_USER_PROFILE" method="post">
				<input type="hidden" name="personalData" value="false">
				<div class="form-floating mb-3">
					<input id="emailInput" type="email" name="email" class="form-control" placeholder="<fmt:message bundle="${i18n}" key="form.email"/>" value="${sessionScope.user.email}">
					<label for="emailInput"><fmt:message bundle="${i18n}" key="form.email"/></label>
				</div>
				<div class="form-floating mb-3">
					<input id="oldPasswordInput" type="password" name="oldPassword" class="form-control" placeholder="<fmt:message bundle="${i18n}" key="form.old.password"/>" autocomplete="new-password">
					<label for="oldPasswordInput"><fmt:message bundle="${i18n}" key="form.old.password"/></label>
				</div>
				<div class="form-floating mb-3">
					<input id="newPasswordInput" type="password" name="newPassword" class="form-control" placeholder="<fmt:message bundle="${i18n}" key="form.new.password"/>">
					<label for="newPasswordInput"><fmt:message bundle="${i18n}" key="form.new.password"/></label>
				</div>
				<c:if test="${sessionScope.errorMessage != null}">
					<div class="text-danger mb-3"><fmt:message bundle="${i18n}" key="${sessionScope.errorMessage}"/></div>
					${sessionScope.remove("errorMessage")}
				</c:if>
				<input class="btn btn-primary btn-lg w-100" type="submit" value="<fmt:message bundle="${i18n}" key="form.submit.btn.save"/>">
			</form>
		</div>
	</div>
</div>
