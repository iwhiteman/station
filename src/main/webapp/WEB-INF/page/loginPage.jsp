<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="appMessages" var="i18n"/>

<div class="container d-flex h-100 align-items-center">
	<div class="row w-100 justify-content-center py-5">
		<div class="col-lg-4">
			<h3 class="fw-light mb-3"><fmt:message bundle="${i18n}" key="form.submit.btn.login"/></h3>
			<form action="pageController?command=LOGIN_USER" method="post">
				<div class="form-floating mb-3">
					<input id="emailInput" type="email" name="email" class="form-control" placeholder="<fmt:message bundle="${i18n}" key="form.email"/>">
					<label for="emailInput"><fmt:message bundle="${i18n}" key="form.email"/></label>
				</div>
				<div class="form-floating mb-3">
					<input id="passwordInput" type="password" name="password" class="form-control" placeholder="<fmt:message bundle="${i18n}" key="form.password"/>">
					<label for="passwordInput"><fmt:message bundle="${i18n}" key="form.password"/></label>
				</div>
				<c:if test="${sessionScope.errorMessage != null}">
					<div class="text-danger mb-3"><fmt:message bundle="${i18n}" key="${sessionScope.errorMessage}"/></div>
					${sessionScope.remove("errorMessage")}
				</c:if>
				<input class="btn btn-primary btn-lg w-100" type="submit" value="<fmt:message bundle="${i18n}" key="form.submit.btn.login"/>">
			</form>
		</div>
	</div>
</div>
