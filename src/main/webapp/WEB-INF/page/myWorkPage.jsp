<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="appMessages" var="i18n"/>

<div class="container mt-5">
	<c:if test="${requestScope.hasOrdersInWork}">
		<div class="row border-end">
			<c:forEach var="orderMap" items="${requestScope.ordersInWork}">
				<div class="col-lg-3 border-start">
					<div class="border-bottom py-2 mb-3">${orderMap.key}</div>
					<c:forEach var="orderVO" items="${orderMap.value}">
						<c:set var="car" value="${orderVO.car}"/>
						<c:set var="order" value="${orderVO.order}"/>
						<a href="pageController?command=EDIT_ORDER_PAGE&orderId=${order.id}" class="alert alert-primary d-block text-decoration-none" role="alert">
							<span>#${order.id} ${car.brand} ${car.model} ${car.year}</span>
						</a>
					</c:forEach>
				</div>
			</c:forEach>
		</div>
	</c:if>
	<c:if test="${!requestScope.hasOrdersInWork}">
		<p class="text-secondary"><fmt:message bundle="${i18n}" key="label.no.data"/></p>
	</c:if>
</div>
