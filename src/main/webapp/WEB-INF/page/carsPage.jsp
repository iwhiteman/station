<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="appMessages" var="i18n"/>

<div class="container">
	<div class="d-flex justify-content-between align-items-baseline mt-5 mb-4">
		<h4 class="fw-light"><fmt:message bundle="${i18n}" key="page.cars.caption"/></h4>
		<a class="btn btn-outline-primary" href="pageController?command=EDIT_CAR_PAGE" role="button"><fmt:message bundle="${i18n}" key="label.add.car"/></a>
	</div>
	<c:set var="hasCars" value="${not empty requestScope.cars}"/>
	<c:if test="${hasCars}">
		<div class="grid">
			<c:forEach var="car" items="${requestScope.cars}">
				<div class="g-col-4">
					<a class="text-decoration-none" style="color:var(--bs-gray-700)" href="pageController?command=CAR_INFO_PAGE&carId=${car.id}" role="button">
						<div class="card">
							<div class="card-body">
								<h5 class="card-title">${car.brand} ${car.model}</h5>
								<p class="card-text">${car.regNumber}</p>
							</div>
						</div>
					</a>
				</div>
			</c:forEach>
		</div>
	</c:if>
	<c:if test="${!hasCars}">
		<p class="text-secondary">
			<fmt:message bundle="${i18n}" key="label.no.data"/></p>
	</c:if>
</div>
