<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="appMessages" var="i18n"/>

<div class="container mt-5">
	<c:set var="car" value="${requestScope.orderVO.car}"/>
	<c:set var="order" value="${requestScope.orderVO.order}"/>
	<c:set var="orderHistory" value="${requestScope.orderVO.orderHistory}"/>
	<nav class="pb-2" aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a class="text-decoration-none" href="pageController?command=CAR_INFO_PAGE&carId=${car.id}" role="button">← <fmt:message bundle="${i18n}" key="label.back"/></a></li>
		</ol>
	</nav>
	<h4 class="fw-light mb-4"><fmt:message bundle="${i18n}" key="page.edit.order.caption"/> ${car.brand} ${car.model} (${car.regNumber})</h4>
	<div class="row">
		<div class="col-lg-4">
			<p><fmt:message bundle="${i18n}" key="form.year"/>: ${car.year}</p>
			<p><fmt:message bundle="${i18n}" key="form.engine.capacity"/>: ${car.engineCapacity}</p>
			<p><fmt:message bundle="${i18n}" key="form.vin.code"/>: ${car.vinCode}</p>
		</div>
		<div class="col-lg-4">
			<p><fmt:message bundle="${i18n}" key="form.mileage"/>: ${order.carMileage}</p>
			<p>
				<fmt:parseDate type="date" var="date" value="${order.startDate}" pattern="yyyy-MM-dd'T'HH:mm"/>
				<fmt:message bundle="${i18n}" key="form.date"/>: <fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${date}"/>
			</p>
			<p><fmt:message bundle="${i18n}" key="form.price"/>: ${order.price} <fmt:message bundle="${i18n}" key="label.currency"/>.</p>
		</div>
		<div class="col-lg-4">
			<p>&nbsp;</p>
			<p>
				<fmt:parseDate type="date" var="endDate" value="${order.endDate}" pattern="yyyy-MM-dd'T'HH:mm"/>
				<fmt:message bundle="${i18n}" key="form.end.date"/>: <fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${endDate}"/>
			</p>
		</div>
	</div>
	<div class="row pt-3">
		<div class="col-lg-6">
			<p class="mb-2"><fmt:message bundle="${i18n}" key="form.description"/></p>
			<pre style="font-family: var(--bs-body-font-family);font-size: var(--bs-body-font-size);margin-bottom: 0;">${order.description}</pre>
		</div>
		<div class="col-lg-6">
			<p class="mb-2"><fmt:message bundle="${i18n}" key="form.master.notes"/></p>
			<pre style="font-family: var(--bs-body-font-family);font-size: var(--bs-body-font-size);margin-bottom: 0;">${order.masterNotes}</pre>
		</div>
	</div>
	<c:if test="${not empty orderHistory}">
		<div class="row">
			<div class="col-lg-12">
				<table class="table table-sm align-middle mt-5">
					<thead>
					<tr>
						<th scope="col"><fmt:message bundle="${i18n}" key="label.status"/></th>
						<th scope="col" class="px-4"><fmt:message bundle="${i18n}" key="label.employee"/></th>
						<th scope="col" class="px-4"><fmt:message bundle="${i18n}" key="label.date.change"/></th>
						<th scope="col"><fmt:message bundle="${i18n}" key="label.cause"/></th>
					</tr>
					</thead>
					<tbody>
					<c:forEach var="history" items="${orderHistory}">
						<tr>
							<td>${history.orderStatus}</td>
							<td class="px-4">${history.user.firstName} ${history.user.lastName} (${history.user.userRole})</td>
							<td class="px-4">
								<fmt:parseDate type="date" var="date" value="${history.date}" pattern="yyyy-MM-dd'T'HH:mm"/>
								<fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${date}"/>
							</td>
							<td>${history.notes}</td>
						</tr>
					</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</c:if>
</div>
