<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="appMessages" var="i18n"/>

<div class="container mt-5">
	<nav class="pb-2" aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a class="text-decoration-none" href="pageController?command=CARS_PAGE" role="button">← <fmt:message bundle="${i18n}" key="label.back"/></a></li>
		</ol>
	</nav>
	<c:set var="car" value="${requestScope.car}"/>
	<div class="d-flex justify-content-between align-items-baseline mb-4">
		<h4 class="fw-light">${car.brand} ${car.model} (${car.regNumber})</h4>
		<div class="d-flex">
			<a class="btn btn-outline-primary mx-3" href="pageController?command=EDIT_CAR_PAGE&carId=${car.id}" role="button"><fmt:message bundle="${i18n}" key="label.edit.car"/></a>
			<a class="btn btn-outline-primary" href="pageController?command=EDIT_ORDER_PAGE&carId=${car.id}" role="button"><fmt:message bundle="${i18n}" key="label.create.order"/></a>
		</div>
	</div>
	<p><fmt:message bundle="${i18n}" key="form.year"/>: ${car.year}</p>
	<p><fmt:message bundle="${i18n}" key="form.engine.capacity"/>: ${car.engineCapacity}</p>
	<p><fmt:message bundle="${i18n}" key="form.vin.code"/>: ${car.vinCode}</p>
	<c:set var="order" value="${requestScope.orders}"/>
	<c:if test="${not empty order}">
		<table class="table table-sm table-hover align-middle mt-4">
			<thead>
			<tr>
				<th scope="col"><fmt:message bundle="${i18n}" key="form.mileage"/></th>
				<th scope="col"><fmt:message bundle="${i18n}" key="form.date"/></th>
				<th scope="col"><fmt:message bundle="${i18n}" key="form.price"/>, <fmt:message bundle="${i18n}" key="label.currency"/>.</th>
				<th scope="col"><fmt:message bundle="${i18n}" key="form.description"/></th>
				<th scope="col"></th>
			</tr>
			</thead>
			<tbody>
			<c:forEach var="order" items="${requestScope.orders}">
				<tr>
					<td>${order.carMileage}</td>
					<td>
						<fmt:parseDate type="date" var="date" value="${order.startDate}" pattern="yyyy-MM-dd"/>
						<fmt:formatDate pattern="dd.MM.yyyy" value="${date}"/>
					</td>
					<td>${order.price}</td>
					<td><pre style="font-family: var(--bs-body-font-family);font-size: var(--bs-body-font-size);margin-bottom: 0;">${order.description}</pre></td>
					<td><a class="btn btn-link" href="pageController?command=VIEW_ORDER_PAGE&orderId=${order.id}" role="button"><fmt:message bundle="${i18n}" key="label.view"/></a></td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
	</c:if>
</div>
