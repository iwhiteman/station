<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="appMessages" var="i18n"/>

<div class="container h-100 d-flex flex-column justify-content-center align-items-center text-center">
	<h1 class="fw-light"><fmt:message bundle="${i18n}" key="page.error.title"/></h1>
	<div class="col-lg-6 mx-auto">
		<p class="lead mb-4"><fmt:message bundle="${i18n}" key="page.error.error.description"/></p>
		<div class="d-grid gap-2 d-sm-flex justify-content-sm-center">
			<a class="btn btn-outline-primary" href="${pageContext.request.contextPath}/pageController?command=MAIN_PAGE" role="button"><fmt:message bundle="${i18n}" key="label.go.to.main.page"/></a>
		</div>
	</div>
</div>
