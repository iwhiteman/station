<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="appMessages" var="i18n"/>

<div class="container mt-5">
	<c:set var="car" value="${requestScope.car}"/>
	<c:if test="${car != null}">
		<nav class="pb-2" aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a class="text-decoration-none" href="pageController?command=CAR_INFO_PAGE&carId=${car.id}" role="button">← <fmt:message bundle="${i18n}" key="label.back"/></a></li>
			</ol>
		</nav>
	</c:if>
	<h4 class="fw-light mb-4"><fmt:message bundle="${i18n}" key="page.edit.car.caption"/></h4>
	<div class="row">
		<div class="col-lg-4">
			<form action="pageController?command=SAVE_CAR" method="post">
				<input type="hidden" name="carId" value="${car.id}">
				<div class="form-floating mb-3">
					<input id="brandInput" type="text" name="brand" class="form-control" placeholder="<fmt:message bundle="${i18n}" key="form.brand"/>" value="${car.brand}">
					<label for="brandInput"><fmt:message bundle="${i18n}" key="form.brand"/></label>
				</div>
				<div class="form-floating mb-3">
					<input id="modelInput" type="text" name="model" class="form-control" placeholder="<fmt:message bundle="${i18n}" key="form.model"/>" value="${car.model}">
					<label for="modelInput"><fmt:message bundle="${i18n}" key="form.model"/></label>
				</div>
				<div class="form-floating mb-3">
					<input id="yearInput" type="number" name="year" class="form-control" placeholder="<fmt:message bundle="${i18n}" key="form.year"/>" value="${car.year}">
					<label for="yearInput"><fmt:message bundle="${i18n}" key="form.year"/></label>
				</div>
				<div class="form-floating mb-3">
					<input id="engineCapacityInput" type="number" name="engineCapacity" class="form-control" placeholder="<fmt:message bundle="${i18n}" key="form.engine.capacity"/>" value="${car.engineCapacity}">
					<label for="engineCapacityInput"><fmt:message bundle="${i18n}" key="form.engine.capacity"/></label>
				</div>
				<div class="form-floating mb-3">
					<input id="vinCodeInput" type="text" maxlength="17" name="vinCode" class="form-control text-uppercase" placeholder="<fmt:message bundle="${i18n}" key="form.vin.code"/>" value="${car.vinCode}">
					<label for="vinCodeInput"><fmt:message bundle="${i18n}" key="form.vin.code"/></label>
				</div>
				<div class="form-floating mb-3">
					<input id="regNumberInput" type="text" name="regNumber" maxlength="10" class="form-control text-uppercase" placeholder="<fmt:message bundle="${i18n}" key="form.reg.number"/>" value="${car.regNumber}">
					<label for="regNumberInput"><fmt:message bundle="${i18n}" key="form.reg.number"/></label>
				</div>
				<c:if test="${sessionScope.errorMessage != null}">
					<div class="text-danger mb-3"><fmt:message bundle="${i18n}" key="${sessionScope.errorMessage}"/></div>
					${sessionScope.remove("errorMessage")}
				</c:if>
				<input class="btn btn-primary btn-lg w-100" type="submit" value="<fmt:message bundle="${i18n}" key="form.submit.btn.save"/>">
			</form>
		</div>
	</div>
</div>
