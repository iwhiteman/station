<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="appMessages" var="i18n"/>

<div class="container d-flex h-100 align-items-center">
	<div class="row w-100 justify-content-center py-5">
		<div class="col-lg-4">
			<h3 class="fw-light mb-3"><fmt:message bundle="${i18n}" key="form.submit.btn.registration"/></h3>
			<%@ include file="../component/registrationForm.jsp" %>
		</div>
	</div>
</div>
