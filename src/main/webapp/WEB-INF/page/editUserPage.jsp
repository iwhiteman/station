<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="appMessages" var="i18n"/>

<div class="container mt-5">
	<nav class="pb-2" aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a class="text-decoration-none" href="pageController?command=TEAM_MANAGEMENT_PAGE" role="button">← <fmt:message bundle="${i18n}" key="label.back"/></a></li>
		</ol>
	</nav>
	<c:set var="user" value="${requestScope.editUser}"/>
	<h4 class="fw-light mb-4">${user.lastName} ${user.firstName}</h4>
	<p><fmt:message bundle="${i18n}" key="form.email"/>: ${user.email}</p>
	<p><fmt:message bundle="${i18n}" key="form.phone.number"/>: ${user.phoneNumber}</p>
	<a class="btn btn-outline-danger mt-4" href="pageController?command=DELETE_USER&userId=${user.id}" role="button"><fmt:message bundle="${i18n}" key="label.delete.user"/></a>
</div>
