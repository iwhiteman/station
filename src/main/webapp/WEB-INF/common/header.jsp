<%@ page contentType="text/html;charset=UTF-8" %>
<header class="fixed-top bg-white">
	<nav class="navbar navbar-expand-lg navbar-light">
		<div class="container">
			<a class="navbar-brand" href="pageController?command=MAIN_PAGE">
				<img class="logo" src="${pageContext.request.contextPath}/public/img/logo.png" alt="Logo"/>
			</a>
			<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
					<c:forEach var="menuItem" items="${requestScope.menuItems}">
						<li class="nav-item">
							<a class="nav-link${menuItem.active ? " active" : ""}" href="pageController?command=${menuItem.name}"><fmt:message bundle="${i18n}" key="${menuItem.title}"/></a>
						</li>
					</c:forEach>
				</ul>
				<c:set var="hasUser" value="${sessionScope.user != null}"/>
				<c:if test="${hasUser}">
					<span class="header-btn"><fmt:message bundle="${i18n}" key="label.welcome"/>, <c:out value="${sessionScope.user.firstName}"/></span>
					<a class="btn btn-outline-primary header-btn" href="pageController?command=LOGOUT_USER" role="button"><fmt:message bundle="${i18n}" key="label.logout"/></a>
				</c:if>
				<c:if test="${!hasUser}">
					<c:if test="${not requestScope.loginPage}">
						<a class="btn btn-outline-primary header-btn" href="pageController?command=LOGIN_PAGE" role="button"><fmt:message bundle="${i18n}" key="label.login"/></a>
					</c:if>
					<c:if test="${not requestScope.registrationPage}">
						<a class="btn btn-outline-primary header-btn" href="pageController?command=REGISTRATION_PAGE" role="button"><fmt:message bundle="${i18n}" key="label.registration"/></a>
					</c:if>
				</c:if>
				<div class="language">
					<a class="language-item${sessionScope.locale eq 'ru' ? " active" : ""}" href="pageController?command=CHANGE_LANGUAGE&locale=ru">рус</a>
					<span> / </span>
					<a class="language-item${sessionScope.locale eq 'en' ? " active" : ""}" href="pageController?command=CHANGE_LANGUAGE&locale=en">eng</a>
				</div>
			</div>
		</div>
	</nav>
</header>
