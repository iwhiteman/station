<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="appMessages" var="i18n"/>

<!DOCTYPE html>
<html class="h-100">
<head>
	<meta charset="utf-8"/>
	<title><fmt:message bundle="${i18n}" key="${requestScope.pageTitle}"/></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="${pageContext.request.contextPath}/public/img/favicon.png">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/public/css/bootstrap.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/public/css/style.css">
</head>
<body class="d-flex flex-column h-100">
	<c:if test="${not requestScope.errorPage}">
		<%@ include file="header.jsp" %>
	</c:if>
	<div class="content">
		<c:url var="bodyUrl" value='../page/${requestScope.pagePath}'/>
		<jsp:include page="${bodyUrl}"/>
	</div>
	<%@ include file="footer.jsp" %>
	<script src="${pageContext.request.contextPath}/public/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/public/js/main.js"></script>
</body>
</html>
