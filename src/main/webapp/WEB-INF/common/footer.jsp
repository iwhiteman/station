<%@ page import="java.util.Calendar" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<div class="container">
	<footer class="d-flex flex-wrap justify-content-between align-items-center py-3 border-top">
		<c:set var="year" value="<%= Calendar.getInstance().get(Calendar.YEAR) %>"/>
		<p class="col-md-4 mb-0 text-muted">&copy; <c:out value="${year}"/> Car Repair</p>
	</footer>
</div>
