CREATE DATABASE station
    DEFAULT CHARACTER SET utf8
	COLLATE utf8_general_ci;

USE station;

CREATE TABLE station.user_status (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(10) NOT NULL,
	PRIMARY KEY (id));

CREATE TABLE station.user_role (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(11) NOT NULL,
	PRIMARY KEY (id));

CREATE TABLE station.users (
	id INT NOT NULL AUTO_INCREMENT,
	first_name VARCHAR(15) NOT NULL,
	last_name VARCHAR(15) NOT NULL,
	phone_number VARCHAR(15),
	email VARCHAR(20) NOT NULL,
	password_hash VARCHAR(60) NOT NULL,
	user_status_id INT NOT NULL,
	user_role_id INT NOT NULL,
	PRIMARY KEY (id),
	INDEX fk_users_user_status_idx (user_status_id ASC) VISIBLE,
	INDEX fk_users_user_role_idx (user_role_id ASC) VISIBLE,
	UNIQUE INDEX email_UNIQUE (email ASC) VISIBLE,
	CONSTRAINT fk_users_user_status
		FOREIGN KEY (user_status_id)
			REFERENCES station.user_status (id)
			ON DELETE CASCADE
			ON UPDATE CASCADE,
	CONSTRAINT fk_users_user_role
		FOREIGN KEY (user_role_id)
			REFERENCES station.user_role (id)
			ON DELETE CASCADE
			ON UPDATE CASCADE);

CREATE TABLE station.order_status (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(10) NOT NULL,
	PRIMARY KEY (id));

CREATE TABLE station.cars (
	id INT NOT NULL AUTO_INCREMENT,
	brand VARCHAR(10) NOT NULL,
	model VARCHAR(10) NOT NULL,
	year INT NOT NULL,
	reg_number VARCHAR(10) NOT NULL,
	engine_capacity INT NOT NULL,
	vin_code VARCHAR(17) NOT NULL,
	user_id INT NOT NULL,
	PRIMARY KEY (id),
	INDEX fk_cars_users_idx (user_id ASC) VISIBLE,
	UNIQUE INDEX vin_code_UNIQUE (vin_code ASC) VISIBLE,
	CONSTRAINT fk_cars_users
		FOREIGN KEY (user_id)
			REFERENCES station.users (id)
			ON DELETE CASCADE
			ON UPDATE CASCADE);

CREATE TABLE station.orders (
	id INT NOT NULL AUTO_INCREMENT,
	create_date DATETIME NOT NULL,
	start_date DATETIME NOT NULL,
	end_date DATETIME,
	mileage INT NOT NULL,
	description VARCHAR(1000) NOT NULL,
	master_notes VARCHAR(1000),
	price INT,
	order_status_id INT NOT NULL,
	car_id INT NOT NULL,
	master_id INT,
	manager_id INT,
	PRIMARY KEY (id),
	INDEX fk_orders_order_status_idx (order_status_id ASC) VISIBLE,
	INDEX fk_orders_cars_idx (car_id ASC) VISIBLE,
	INDEX fk_orders_manger_idx (manager_id ASC) VISIBLE,
	INDEX fk_orders_master_idx (master_id ASC) VISIBLE,
	CONSTRAINT fk_orders_order_status
		FOREIGN KEY (order_status_id)
			REFERENCES station.order_status (id)
			ON DELETE CASCADE
			ON UPDATE CASCADE,
	CONSTRAINT fk_orders_cars
		FOREIGN KEY (car_id)
			REFERENCES station.cars (id)
			ON DELETE CASCADE
			ON UPDATE CASCADE,
	CONSTRAINT fk_orders_manager
		FOREIGN KEY (manager_id)
			REFERENCES station.users (id)
			ON DELETE CASCADE
			ON UPDATE CASCADE,
	CONSTRAINT fk_orders_master
		FOREIGN KEY (master_id)
			REFERENCES station.users (id)
			ON DELETE CASCADE
			ON UPDATE CASCADE);

CREATE TABLE station.orders_progress (
	id INT NOT NULL AUTO_INCREMENT,
	date DATETIME NOT NULL,
	notes VARCHAR(1000) NULL,
	order_id INT NOT NULL,
	order_status_id INT NOT NULL,
	user_id INT NOT NULL,
	PRIMARY KEY (id),
	INDEX fk_orders_progress_orders_idx (order_id ASC) VISIBLE,
	INDEX fk_orders_progress_order_status_idx (order_status_id ASC) VISIBLE,
	INDEX fk_orders_progress_users_idx (user_id ASC) VISIBLE,
	CONSTRAINT fk_orders_progress_orders
		FOREIGN KEY (order_id)
			REFERENCES station.orders (id)
			ON DELETE CASCADE
			ON UPDATE CASCADE,
	CONSTRAINT fk_orders_progress_order_status
		FOREIGN KEY (order_status_id)
			REFERENCES station.order_status (id)
			ON DELETE CASCADE
			ON UPDATE CASCADE,
	CONSTRAINT fk_orders_progress_users
		FOREIGN KEY (user_id)
			REFERENCES station.users (id)
			ON DELETE CASCADE
			ON UPDATE CASCADE);

INSERT INTO station.user_status (name) VALUES ('в штате');
INSERT INTO station.user_status (name) VALUES ('уволен');

INSERT INTO station.user_role (name) VALUES ('собственник');
INSERT INTO station.user_role (name) VALUES ('менеджер');
INSERT INTO station.user_role (name) VALUES ('мастер');
INSERT INTO station.user_role (name) VALUES ('клиент');

INSERT INTO station.order_status (name) VALUES ('открыт');
INSERT INTO station.order_status (name) VALUES ('согласован');
INSERT INTO station.order_status (name) VALUES ('в работе');
INSERT INTO station.order_status (name) VALUES ('на паузе');
INSERT INTO station.order_status (name) VALUES ('выполнен');
INSERT INTO station.order_status (name) VALUES ('закрыт');
INSERT INTO station.order_status (name) VALUES ('отменен');
